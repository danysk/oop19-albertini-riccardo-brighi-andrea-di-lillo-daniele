package testplayer;

import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import model.player.Player;
import model.player.PlayerBuilderImpl;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public final class TestPlayersCollection {

    private PlayersCollection playersList;

    void init() throws IOException {
        playersList = new PlayersCollectionImpl();
        this.playersList.addPlayer("Daniele");
        this.playersList.addPlayer("Andrea");
        this.playersList.addPlayer("Riccardo");
        this.playersList.addPlayer("Gianluca");
        this.playersList.addPlayer("Alex");

        playersList.update();
    }

    @Test
    void testInsertionsBestScore() throws IOException {
        init();
        if (this.playersList.getPlayer("Daniele").isEmpty()) {
            throw new IllegalStateException();
        }
        this.playersList.getPlayer("Daniele").get().setNewScore(50);
        if (this.playersList.getPlayer("Andrea").isEmpty()) {
            throw new IllegalStateException();
        }
        this.playersList.getPlayer("Andrea").get().setNewScore(40);
        if (this.playersList.getPlayer("Riccardo").isEmpty()) {
            throw new IllegalStateException();
        }
        this.playersList.getPlayer("Riccardo").get().setNewScore(15);
        if (this.playersList.getPlayer("Gianluca").isEmpty()) {
            throw new IllegalStateException();
        }
        this.playersList.getPlayer("Gianluca").get().setNewScore(30);
        if (this.playersList.getPlayer("Alex").isEmpty()) {
            throw new IllegalStateException();
        }
        this.playersList.getPlayer("Alex").get().setNewScore(25);
        assertEquals(this.playersList.getBestPlayer().get().getName(), this.playersList.getPlayer("Daniele").get().getName());
    }

    @Test
    void testMultiplePlayerWithSameName() throws IOException {
        init();
        assertThrows(IllegalStateException.class, () -> {
            this.playersList.addPlayer("Daniele");
        });
    }

    @Test
    void testCurrentPlayer() throws IOException {
        init();
        this.playersList.setCurrentPlayer("Riccardo");
        assertEquals(this.playersList.getCurrentPlayer().get().getName(), this.playersList.getPlayer("Riccardo").get().getName());
        this.playersList.logout();
        assertEquals(this.playersList.getCurrentPlayer(), Optional.empty());
    }

    @Test
    void testUpdatePlayersData() throws IOException, ParseException {
        init();
        final Optional<Player> p = this.playersList.getPlayer("Alex");
        if(p.isEmpty()) {
            throw new IllegalStateException();
        }
        p.get().setNewScore(30);
        this.playersList.update();
        this.playersList = PlayersCollectionImpl.getCollectionFromDisk();
        assertEquals(this.playersList.getPlayer("Alex").get().getBestScore(), 30);
    }

    @Test
    void testContainsPlayer() throws IOException {
        init();
        assertTrue(this.playersList.containsPlayer("DANIELE"));
        assertFalse(this.playersList.containsPlayer("Gianfrancioschio"));
        assertTrue(this.playersList.containsPlayer("RICCARDO"));
    }

    @Test
    void testLogout() throws IOException, ParseException {
        init();
        this.playersList.setCurrentPlayer("Daniele");
        assertEquals(playersList.getCurrentPlayerName(), "DANIELE");
        this.playersList.logout();
        this.playersList = PlayersCollectionImpl.getCollectionFromDisk();
        assertEquals(playersList.getCurrentPlayerName(), "");
    }

    @Test
    void testGetPlayersInOrder() throws IOException {
        init();
        this.playersList.reset();
        this.playersList.update();
        final Player p1 = new PlayerBuilderImpl().addNamePlayer("Filippo").addBestScore(50).build();
        final Player p2 = new PlayerBuilderImpl().addNamePlayer("Marco").addBestScore(40).build();
        final Player p3 = new PlayerBuilderImpl().addNamePlayer("Jack").addBestScore(80).build();
        this.playersList.addPlayer(p1);
        this.playersList.addPlayer(p2);
        this.playersList.addPlayer(p3);
        final Optional<List<Player>> list = this.playersList.getPlayersInOrder();
        if (list.isEmpty()) {
            return;
        }
        final List<Player> orderedList = new ArrayList<Player>();
        orderedList.add(p3);
        orderedList.add(p1);
        orderedList.add(p2);
        assertEquals(list.get(), orderedList);
    }
}
