package testLaunch;

import view.ArrowImplVector;
import element.Vector2D;
import element.Vector2DImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


public class TestLaunchCoordinates {

    ArrowImplVector a = new ArrowImplVector();

    @Test
    void zero() {
        this.a.setEnd(new Vector2DImpl(0, 0));
    }

    @Test
    void lotOfVector() {
        for (int i = -100; i < 100; i++) {
            for (int j = -100; j < 100; j++) {
                Vector2D v = new Vector2DImpl(i, j);
                this.a.setEnd(v);
            }
        }
    }

}
