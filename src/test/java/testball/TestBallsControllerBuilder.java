package testball;

import controller.ballscontroller.*;
import controller.ballscontroller.time.TimeConverterUnit;
import controller.ballscontroller.time.TimeType;
import element.Elements;
import model.ball.BallBuilderImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestBallsControllerBuilder {

    @Test
    void testError() {
        final BallsControllerBuilder builder = new BallsControllerBuilderImpl();
        assertThrows(IllegalArgumentException.class, () -> builder.addPause(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addEndLaunch(null));
        assertThrows(IllegalStateException.class, builder::build);
        assertThrows(IllegalArgumentException.class, () -> builder.addBallsBuilder(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addInitialBalls(-1));
        assertThrows(IllegalArgumentException.class, () -> builder.addNumberOfStepBetweenBalls(-1));
        assertThrows(IllegalArgumentException.class, () -> builder.addStartPosition(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addType(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addTime(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addMinusAngleDegrees(120));
        assertThrows(IllegalArgumentException.class, () -> builder.addMinusAngleRadians(3));
        assertThrows(IllegalArgumentException.class, () -> builder.addTimeMeasures(null));
    }

    @Test
    void testErrorAfterType() {
        final BallsControllerBuilder builder = new BallsControllerBuilderImpl()
                .addType(BallsControllerBuilder.Type.SINGLE_THREAD);
        assertThrows(IllegalArgumentException.class, () -> builder.addPause(null));
        assertThrows(IllegalArgumentException.class, () -> builder.addTime(null));
    }

    @Test
    void testBuilder() {
        new BallsControllerBuilderImpl()
                .addBallsBuilder(new BallBuilderImpl()
                        .addDamage(1)
                        .addStartSpeed(0.1)
                        .addRadius(1))
                .addInitialBalls(1)
                .addType(BallsControllerBuilder.Type.SINGLE_THREAD)
                .addPause(BallsControllerBuilder.Pause.PAUSE)
                .addTime(TimeType.ABSTRACT_TIME)
                .addTimeInterval(20)
                .addStartPosition(Elements.ORIGIN)
                .addMinusAngleDegrees(20)
                .addTimeMeasures(TimeConverterUnit.SECOND)
                .build();
    }
}
