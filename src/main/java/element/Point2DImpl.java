package element;

/**
 * Class that represent the point2D.
 *
 */
public class Point2DImpl implements Point2D {

    private final double x;
    private final double y;

    /**
     * 
     * @param x x coordinate of the point
     * @param y y coordinate of the point
     */
    public Point2DImpl(final double x, final double y) {
        final int precision = 6;
        this.x = Elements.round(x, precision);
        this.y = Elements.round(y, precision);
    }

    /**
     * @return the x component of the vector
     */
    @Override
    public double getX() {
        return this.x;
    }

    /**
     * @return the y component of the vector
     */
    @Override
    public double getY() {
        return this.y;
    }

    /**
     * @param v the v of the direction to sum
     * @return a new point that is the current point summed to the vector v
     */
    @Override
    public Point2D sum(final Vector2D v) {
        return new Point2DImpl(this.getX() + v.getXComponent(), this.getY() + v.getYComponent());
    }

    /**
     * @param p the other point
     * @return a new vector which is the subtraction of the two points' coordinates (x, y)
     */
    @Override
    public Vector2D subtraction(final Point2D p) {
        return new Vector2DImpl(this.getX() - p.getX(), this.getY() - p.getY());
    }

    /**
     * @param p the other point
     * @return the distance between the two points
     */
    @Override
    public double distance(final Point2D p) {
        return this.subtraction(p).getModulus();
    }

    /**
     *
     * @return  a string that represent the point
     */
    @Override
    public String toString() {
        return "Point2D ( " + this.x + ", " + this.y + " )";
    }

    /**
     *
     * @param o the other object
     * @return true if the two point have the same coordinate
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Point2DImpl)) {
            return false;
        }

        final Point2DImpl point2D = (Point2DImpl) o;

        if (Double.compare(point2D.getX(), this.getX()) != 0) {
            return false;
        }
        return Double.compare(point2D.getY(), this.getY()) == 0;
    }

    /**
     *
     * @return a int that represent the hash of the point
     */
    @Override
    public int hashCode() {
        final int hash = 31;
        int result;
        long temp;
        temp = Double.doubleToLongBits(this.getX());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(this.getY());
        result = hash * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
