package view.ball;

import element.Point2D;

/**
 * Interface for the View of the Ball.
 *
 */
public interface BallView {

    /**
     * @return the radius of the ball
     */
    double getRadius();

    /**
     * @return the position of the ball
     */

    Point2D getPosition();
}
