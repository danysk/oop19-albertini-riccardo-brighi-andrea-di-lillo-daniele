package view.ball;

import element.Point2D;
import element.Point2DImpl;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import utility.BallColor;

import java.util.Objects;

/**
 * Factory to create BallView based on circle.
 *
 */
public class BallViewFactoryCircle implements BallViewFactory<BallViewFactoryCircle.BallViewCircle> {

    /**
     * The BallView with Circle.
     *
     */
    public static final class BallViewCircle extends Circle implements BallView {

        private BallViewCircle(final Point2D point2D, final double radius, final Color color) {
            super(point2D.getX(), point2D.getY(), radius, color);
        }

        @Override
        public Point2D getPosition() {
            return new Point2DImpl(this.getCenterX(), this.getCenterY());
        }
    }

    /**
     * @param position the position of the ball
     * @param radius   the radius of the ball
     * @param color    the color of the ball
     * @return a BallView that is a Circle ( JavaFX) and BallView
     * @throws IllegalArgumentException if position or the color is null, the radius
     *                                  is minus or equal to 0 or any of the
     *                                  coordinate of the positoin is negative
     */
    @Override
    public BallViewCircle standardBalls(final Point2D position, final double radius, final BallColor color) {
        if (Objects.isNull(position) || radius <= 0 || Objects.isNull(color) || position.getY() < 0
                || position.getX() < 0) {
            throw new IllegalArgumentException();
        }
        return new BallViewCircle(position, radius, color.getColor());
    }
}
