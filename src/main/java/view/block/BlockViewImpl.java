package view.block;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import model.block.Block;
import model.block.BlockImpl;

/**
 * This class implements BlockView and used for graphic block
 */
public class BlockViewImpl extends Rectangle implements BlockView {

    private final Block block;
    private final Text text;
    private static double blockStroke = 3;
    private Color colorValue;

    private static final double RED_MAX = 10;
    private static final double YELLOW_MIN = 9;
    private static final double YELLOW_MAX = 20;
    private static final double PURPLE_MIN = 19;
    private static final double PURPLE_MAX = 30;
    private static final double GREEN_MIN = 29;
    private static final double GREEN_MAX = 40;
    private static final double ORANGE_MIN = 39;
    private static final double ORANGE_MAX = 50;
    private static final double LIGHTBLUE_MIN = 49;
    private static final double LIGHTBLUE_MAX = 60;
    private static final double BLUE_MIN = 59;
    private static final double BLUE_MAX = 70;
    private static final double FUCHSIA_MIN = 69;

    private static final double TEXT_X = 15;
    private static final double TEXT_XDEFAULT = 20;
    private static final double TEXT_Y = 30;
    private static final double TEXT_XCOORDINATION = 5;

    public BlockViewImpl(final double x, final double y, final int number) {
        this.setStrokeWidth(this.blockStroke);

        this.block = new BlockImpl(number, this.getStrokeWidth());
        this.text = new Text();

        this.setX(x + this.getStrokeWidth() / 2);
        this.setY(y + this.getStrokeWidth() / 2);

        this.setWidth(this.getBlock().getBlockSize());
        this.setHeight(this.getBlock().getBlockSize());

        this.setBlockColor();
        this.setTextCoordination(x, y, number);
        this.setTextFormat();
    }

    private void setTextCoordination(final double x, final double y, final int number) {
        if (number >= 100) {
            this.text.setX(x + 10);
        } else if (number >= 10) {
            this.text.setX(x + TEXT_X);
        } else {
            this.text.setX(x + TEXT_XDEFAULT);
        }

        this.text.setY(y + TEXT_Y);
    }

    private void updateTextCoordination() {
        if (this.getBlock().getValueBlock() < 100 && (this.getX() - this.blockStroke / 2) == (this.text.getX() - 10)
                || this.getBlock().getValueBlock() < 10 && (this.getX() - this.blockStroke / 2) == this.text.getX() - TEXT_X) {
            this.text.setX(this.text.getX() + TEXT_XCOORDINATION);
        }
    }

    /**
     * @param control it's used to understand if we have a hit o a new line (different brighten)
     * this method permit to change the brightness color of block
     */
    @Override
    public void updateBrightnessColor(final boolean control) {
        if (control) {
            this.colorValue = this.colorValue.desaturate().desaturate().brighter().brighter();
        } else {
            this.colorValue = this.colorValue.desaturate().brighter();
        }

        this.setStroke(this.colorValue);
        this.text.setFill(this.colorValue);
    }

    private void setTextFormat() {
        this.text.setText(Integer.toString(this.block.getValueBlock()));
        this.text.setFill(this.colorValue);
        this.text.setStyle("-fx-font: 18 arial;");

        this.updateTextCoordination();
    }

    private void setBlockColor() {
        if (this.block.getValueBlock() <= RED_MAX && this.colorValue != Color.RED) {
            this.setColorValue(Color.RED);       //rosso
        } else if (this.block.getValueBlock() > YELLOW_MIN && this.block.getValueBlock() <= YELLOW_MAX && this.colorValue != Color.YELLOW) {
            this.setColorValue(Color.YELLOW);    //giallo
        } else if (this.block.getValueBlock() > PURPLE_MIN && this.block.getValueBlock() <= PURPLE_MAX && this.colorValue != Color.PURPLE) {
            this.setColorValue(Color.PURPLE);    //viola
        } else if (this.block.getValueBlock() > GREEN_MIN && this.block.getValueBlock() <= GREEN_MAX && this.colorValue != Color.GREEN) {
            this.setColorValue(Color.GREEN);     //verde
        } else if (this.block.getValueBlock() > ORANGE_MIN && this.block.getValueBlock() <= ORANGE_MAX && this.colorValue != Color.ORANGE) {
            this.setColorValue(Color.ORANGE);    //arancione
        } else if (this.block.getValueBlock() > LIGHTBLUE_MIN && this.block.getValueBlock() <= LIGHTBLUE_MAX && this.colorValue != Color.LIGHTBLUE) {
            this.setColorValue(Color.LIGHTBLUE); //azzurro
        } else if (this.block.getValueBlock() > BLUE_MIN && this.block.getValueBlock() <= BLUE_MAX && this.colorValue != Color.BLUE) {
            this.setColorValue(Color.BLUE);      //blu
        } else if (this.block.getValueBlock() > FUCHSIA_MIN && this.colorValue != Color.FUCHSIA) {
            this.setColorValue(Color.FUCHSIA);   //fuchsia
        }
    }

    private void setColorValue(final Color color) {
        this.colorValue = color;
        this.setStroke(this.colorValue);
    }

    /**
     * @return the Text of value block
     * getter of Text
     */
    @Override
    public Text getText() {
        return this.text;
    }

    /**
     * @return the 'logic' block
     * getter of 'logic' block
     */
    @Override
    public Block getBlock() {
        return this.block;
    }

    /**
     * @return true if the block has been deleted (value minor or equal to 0), false otherwise
     */
    @Override
    public boolean deletedBlock() {
        return this.block.getValueBlock() <= 0;
    }

    /**
     * @param value amount of hit
     * this method is called when a block get hitted, he change color and block value
     */
    public void isHitted(final int value) {
        this.block.setValueBlock(this.block.getValueBlock() - value);
        this.setBlockColor();
        this.updateBrightnessColor(true);
        this.setTextFormat();
    }

}
