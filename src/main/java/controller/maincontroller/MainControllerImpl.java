package controller.maincontroller;

import controller.ballscontroller.BallsControllerBuilderImpl;
import controller.ballscontroller.BallsController;
import controller.ballscontroller.BallsControllerBuilder;
import controller.ballscontroller.time.TimeConverterUnit;
import controller.ballscontroller.time.TimeType;
import controller.viewcontroller.ViewGameController;
import element.Vector2D;
import element.Point2D;
import element.Point2DImpl;
import element.Elements;
import javafx.scene.shape.Shape;
import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import model.collision.CollisionDetected;
import model.collision.CollisionManager;
import model.collision.CollisionDetectedImpl;
import model.collision.CollisionManagerImpl;
import model.player.Player;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;
import utility.BallColor;
import utility.GameThreadImpl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class implements the MainController interface, modelling the core of the game.
 */
public final class MainControllerImpl implements MainController {

    /**
     * This enum models the game state during the session.
     */
    enum GameState {
        PAUSED,
        WAIT_END_LAUNCH,
        READY_TO_LAUNCH
    }

    /**
     * Represent the top border of the game.
     */
    private static final int TOP = 538;
    /**
     * Represent the bottom border of the game.
     */
    private static final int BOTTOM = 110;
    /**
     * Represent the right border of the game.
     */
    private static final int RIGHT = 0;
    /**
     * Represent the left border of the game.
     */
    private static final int LEFT = 350;
    /**
     * Represent the x coordinates of first launch.
     */
    private static final int START_X = 150;
    /**
     * Represent the y coordinates of first launch.
     */
    private static final int START_Y = 410;
    /**
     * Represent minus angle of launch.
     */
    private static final double MINUS_ANGLE_DEGREE = -170.0;
    /**
     * Represent the start speed of the ball.
     */
    private static final int START_SPEED = 150;
    /**
     * Represent the limit speed of the ball.
     */
    private static final int LIMIT_SPEED = 180;
    /**
     * Represent the increment step for the speed.
     */
    private static final int INCREMENT_STEP = 50;
    /**
     * Represent the initial number of balls.
     */
    private static final int INITIAL_BALLS = 1;
    /**
     * Represent the step between the balls.
     */
    private static final int STEP_BETWEEN_BALLS = 1;
    /**
     * Represent the time interval for the BallsController.
     */
    private static final int TIME_INTERVAL = 20;

    private final Player player;
    private final BallsController ballsController;
    private GameState gameState;
    private GameState oldGameState;
    private final ViewGameController viewController;
    private final GameThreadImpl gameThread;
    private final CollisionManager collManager;
    private int moneyEarned;

    public MainControllerImpl(final ViewGameController viewController) throws IOException, ParseException {
        final Optional<Player> optionalPlayer = PlayersCollectionImpl.getCollectionFromDisk().getCurrentPlayer();
        if (optionalPlayer.isEmpty()) {
            throw new IllegalStateException();
        }
        this.player = optionalPlayer.get();
        final int damage = player.getCurrentBallDamage();
        this.moneyEarned = 0;
        this.viewController = viewController;
        this.collManager = new CollisionManagerImpl(getBlocks(), TOP, LEFT, BOTTOM, RIGHT);

        final BallBuilder.Movement checkCollision = (arg) -> {
            if (!arg.isAbleToCollide()) {
                return;
            }
            collManager.updateBlocks(getBlocks());
            Optional<CollisionDetected> cdO = collManager.checkCollision(arg);
            if (cdO.isPresent()) {
                CollisionDetectedImpl cd = (CollisionDetectedImpl) cdO.get();
                if (cd.getBlocks().isPresent() && cd.getNewCenterPosition().isEmpty() && cd.getNewDirection().isEmpty()) {
                    if (arg.isInsideAnotherObject()) {
                        return;
                    }
                    viewController.blockHit(cd.getBlocks().get(), 0);
                    arg.insideAnotherObject();
                }
                if (cd.getBlocks().isPresent() && cd.getNewCenterPosition().isPresent() && cd.getNewDirection().isPresent()) {
                    arg.collision(cd.getNewCenterPosition().get(), cd.getNewDirection().get());
                    notifyHittedBlocks(cd.getBlocks().get());
                }
                if (cd.getBlocks().isEmpty() && cd.getNewCenterPosition().isPresent() && cd.getNewDirection().isPresent()) {
                    arg.collision(cd.getNewCenterPosition().get(), cd.getNewDirection().get());
                }
                if (cd.getBlocks().isEmpty() && cd.getNewCenterPosition().isPresent() && cd.getNewDirection().isEmpty()) {
                    arg.collision(cd.getNewCenterPosition().get(), Elements.VECTOR_NULL);
                }
            } else {
                arg.notInsideAnotherObject();
            }
        };

        final Runnable endLaunchControl = () -> {
            try {
                allBallsBackToHome();
            } catch (IOException e) {
                Logger.error("ERROR_MAINCONTROLLER! Error occurred while balls turning back!");
                e.printStackTrace();
            }
        };

        ballsController = new BallsControllerBuilderImpl().addType(BallsControllerBuilder.Type.MULTI_THREAD)
                .addBallsBuilder(new BallBuilderImpl()
                        .addDamage(damage)
                        .addRadius(getRadius())
                        .addMovement(checkCollision)
                        .addStartSpeed(START_SPEED)
                        .addSpeedLimit(LIMIT_SPEED)
                        .addIncrementSpeedStep(INCREMENT_STEP)
                )
                .addEndLaunch(endLaunchControl)
                .addPause(BallsControllerBuilder.Pause.PAUSE)
                .addInitialBalls(INITIAL_BALLS)
                .addMinusAngleDegrees(MINUS_ANGLE_DEGREE)
                .addNumberOfStepBetweenBalls(STEP_BETWEEN_BALLS)
                .addTime(TimeType.EFFECTIVE_TIME)
                .addTimeMeasures(TimeConverterUnit.SECOND)
                .addStartPosition(new Point2DImpl(START_X, TOP - getRadius()))
                .addTimeInterval(TIME_INTERVAL)
                .build();
        this.gameThread = new GameThreadImpl() {
            @Override
            public void run() {
                while (!this.isEndGame()) {
                    if (!this.isPause()) {
                        try {
                            sleep(TIME_INTERVAL);
                            newGameCycle();
                        } catch (InterruptedException e) {
                            Logger.info("INFO_MAINCONTROLLER! Main thread is interrupted");
                        }
                    } else {
                        Logger.info("INFO_MAINCONTROLLER! Game is paused");
                        this.waitEndPause();
                    }
                }
            }
        };

        this.gameState = GameState.READY_TO_LAUNCH;
    }

    @Override
    public double getMinusAngleDegrees() {
        return MINUS_ANGLE_DEGREE;
    }

    @Override
    public BallColor getColor() {
        return this.player.getCurrentColor();
    }

    @Override
    public int getRadius() {
        return this.player.getCurrentBallRadius();
    }

    @Override
    public Point2D getBallsStartPosition() {
        return new Point2DImpl(START_X, TOP - getRadius());
    }

    @Override
    public Optional<Point2D> getStartPosition() {
        if (this.ballsController.getBalls().stream().findAny().isEmpty()) {
            throw new IllegalStateException();
        }
        if (this.isLaunchPossible()) {
            return Optional.of(new Point2DImpl(this.ballsController.getBalls().stream().findAny().get().getPosition().getX(), START_Y + getRadius()));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public int getEarnedMoney() {
        return this.moneyEarned;
    }

    @Override
    public int getPlayerBestScore() {
        return player.getBestScore();
    }

    @Override
    public void launch() {
        if (this.gameThread.isStarted()) {
            this.gameThread.resumeGame();
        } else {
            this.gameThread.start();
        }
        this.gameState = GameState.WAIT_END_LAUNCH;
    }

    /**
     *
     * @return Return true if launch is possible, false otherwise.
     */
    private boolean isLaunchPossible() {
        return this.gameState == GameState.READY_TO_LAUNCH;
    }

    @Override
    public void notifyHittedBlocks(final List<Shape> blocks) {
        final int damage = player.getCurrentBallDamage();
        this.viewController.blockHit(blocks, damage);
    }

    @Override
    public void incrementMoney() {
        this.moneyEarned++;
    }

    @Override
    public void newGameCycle() {
        this.viewController.drawBalls(this.ballsController.getBalls().stream().map(Ball::getPosition).collect(Collectors.toSet()));
    }

    @Override
    public void addBall() {
        this.ballsController.addBall();
    }

    @Override
    public void setPositionLaunch(final Vector2D vector) {
        if (ballsController.isValidLaunch(vector) && this.isLaunchPossible()) {
            this.launch();
            this.ballsController.launchBalls(vector);
        } else {
            Logger.info("INFO_MAINCONTROLLER! Incorrect vector launch or impossible to launch");
        }
    }

    @Override
    public Set<Ball> getBalls() {
        return this.ballsController.getBalls();
    }

    @Override
    public List<Shape> getBlocks() {
        return this.viewController.getMyBlock();
    }

    /**
     * This method is called when all balls are came back to the start point.
     *
     * @throws IOException Throws IOException if any error occurred
     */
    private void allBallsBackToHome() throws IOException {
        this.newGameCycle();
        this.gameThread.pauseGame();
        this.gameState = GameState.READY_TO_LAUNCH;
        viewController.newLine();
    }

    @Override
    public void gameOver() {
        this.gameThread.endThread();
    }

    @Override
    public void pauseGame() {
        this.oldGameState = this.gameState;
        this.gameState = GameState.PAUSED;
        this.gameThread.pauseGame();
        this.ballsController.pause();
    }

    @Override
    public void restartGame() {
        this.gameState = this.oldGameState;
        this.gameThread.resumeGame();
        this.ballsController.restart();
    }
}
