package controller.gamecontroller;

import controller.maincontroller.MainController;
import controller.viewcontroller.ViewGameController;
import controller.viewcontroller.ViewGameControllerImpl;
import element.Point2DImpl;
import javafx.scene.shape.Shape;
import model.bonusblock.BonusBlock;
import model.bonusblock.BonusFactory;
import model.bonusblock.BonusFactoryImpl;
import model.bonusblock.Strategy;
import view.block.BlockViewImpl;
import view.bonus.BonusViewImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ArrayList;

/**
 * The Controller used for static game object
 */
public class GameControllerImpl implements GameController {

    private MainController mainController;

    private ViewGameController viewGameController;

    private BonusFactory bonus;

    private static int maxBlockOnLine = 7;
    private static double differenceBonusNormal = 25;
    private static double headerY = 120;
    private int bonusProbability = DEFAULT_BONUS_PROB;

    private static final double DEFAULT_X_STEP = 50;
    private static final int DEFAULT_BONUS_PROB = 5;

    /**
     * @return value of maxBlockOnLine
     * this method is a getter of maxBlockOnLine.
     */
    public int getMaxBlockOnLine() {
        return maxBlockOnLine;
    }

    /**
     * @return value of differenceBonusNormal
     * this method is a getter of differenceBonusNormal.
     */
    public double getDifferenceBonusNormal() {
        return differenceBonusNormal;
    }
    /**
     * @return value of headerY
     * this method is a getter of headerY.
     */
    public double getHeaderY() {
        return headerY;
    }

    /**
     * @param bonusProbability probability of Bonus
     * this method is a setter of bonusProbability.
     */
    public void setBonusProbability(final int bonusProbability) {
        this.bonusProbability = bonusProbability;
    }
    public GameControllerImpl() { }

    public GameControllerImpl(final MainController mainController, final ViewGameControllerImpl viewGameController) {
        this.mainController = mainController;
        this.viewGameController = viewGameController;

        Map<BonusFactory.BonusType, Strategy<BonusBlock>> bonusMap = new HashMap<>() {
            {
                put(BonusFactory.BonusType.ADDBALL, (argument) -> {
                    addBall();
                });
                put(BonusFactory.BonusType.COIN, (argument) -> {
                    addCoin();
                });
                put(BonusFactory.BonusType.HORIZONTAL, (argument) -> {
                    viewGameController.rowHit(argument.getPosition().getY());
                });
                put(BonusFactory.BonusType.VERTICAL, (argument) -> {
                    viewGameController.columnHit(argument.getPosition().getX());
                });
            }
        };

        bonus = new BonusFactoryImpl(bonusMap);
    }

    /**
     * @return true if yes, false if no
     * this method tells us if it's time to create a new block or not.
     */
    public boolean setNormalBlock() {
        return new Random().nextInt(100) % 2 == 0;
    }

    /**
     * @return true if yes, false if no
     * this method tells us if it's time to create a new bonus block or not.
     */
    public boolean setBonusBlock() {
        return new Random().nextInt(100) % bonusProbability == 0;
    }

    /**
     * @param x position x of the normal block
     * @param y position y of the normal block
     * @param amount how much block got hit
     * @return new normal block
     * allows you to create a new normal block.
     */
    public Shape newNormalBlock(final double x, final double y, final int amount) {
        return new BlockViewImpl(x, y, amount);
    }

    /**
     * @param x position x of the bonus block
     * @param y position y of the bonus block
     * @return new bonus block
     * allows you to create a new bonus block.
     */
    public Shape newBonusBlock(final double x, final double y) {
        return BonusViewImpl.createBonusView(bonus.random(new Point2DImpl(x, y)));
    }

    /**
     * @param value block value of line
     * @return list of block
     * create new line of block (normal and bonus).
     */
    public List<Shape> newBlockLine(final int value) {
        List<Shape> tmpLine = new ArrayList<>();
        for (double x = 0; x < (this.maxBlockOnLine) * DEFAULT_X_STEP; x += DEFAULT_X_STEP) {
            if (this.setBonusBlock() && x != 0 && x != (this.maxBlockOnLine - 1) * DEFAULT_X_STEP) {
                tmpLine.add(this.newBonusBlock(x + this.differenceBonusNormal, DEFAULT_X_STEP + this.headerY + this.differenceBonusNormal));
            } else if (this.setNormalBlock()) {
                tmpLine.add(this.newNormalBlock(x, DEFAULT_X_STEP + this.headerY, value));
            }
        }

        if (tmpLine.size() == 0) {
            tmpLine.add(this.newNormalBlock(0, DEFAULT_X_STEP + this.headerY, value));
        }
        this.setBonusProbability(DEFAULT_BONUS_PROB);
        return tmpLine;
    }

    /**
     * @return true if lose, false otherwise
     * control if the match is finished.
     */
    public boolean checkLose() {
        final int lastRow = 7;
        for (Shape block : this.viewGameController.getMyBlock()) {
            if (block instanceof BlockViewImpl) {
                if (this.viewGameController.getPosition(block).getY() == ((lastRow * DEFAULT_X_STEP) + this.headerY + (block).getStrokeWidth() / 2)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * this method calls the method of maincontroller
     * and add coin for the player.
     */
    public void addCoin() {
        this.mainController.incrementMoney();
    }

    /**
     * this method calls the method of maincontroller
     * and add ball for the player.
     */
    public void addBall() {
        this.mainController.addBall();
    }

}
