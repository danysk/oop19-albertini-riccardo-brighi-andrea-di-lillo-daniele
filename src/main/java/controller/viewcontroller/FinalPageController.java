package controller.viewcontroller;

import javafx.animation.Animation;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;
import utility.ImageEffects;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class defines a controller for the endGamePage.fxml, managing its events.
 */
public final class FinalPageController implements Initializable {

    /**
     * This is the minimum and maximum angle for the rotation animation.
     */
    private static final int ANGLE = 5;
    /**
     * This is the animation duration in seconds.
     */
    private static final int DURATION = 1;

    @FXML
    private ImageView restartButtonnn;

    @FXML
    private ImageView stats;

    @FXML
    private ImageView btnHome;

    @FXML
    private Pane mainPageContent;

    @FXML
    private Label lblMonete;

    @FXML
    private Label lblMonetineAggiuntive;

    @FXML
    private Label lblPunteggio;

    @FXML
    private Label lblStateMessage;

    @FXML
    private ImageView btnAddBall;

    @FXML
    private Label lblBestScore;

    private PlayersCollection listPlayers;

    /**
     * This method is called to open the leaderboard page.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void openLeaderboard() throws IOException {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/leaderboard.fxml"));
        final Pane pane = loader.load();
        final LBController controller = loader.getController();

        mainPageContent.getChildren().setAll(pane);
        controller.init(1);

        Logger.info("PAGE CHANGING: Changed to leaderboard page from game over page");
    }

    /**
     * This method is called to turn back to the main page.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void btnHomeOnClick() throws IOException {
        final Parent wheelViewParent = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        final Scene wheelViewScene = new Scene(wheelViewParent);
        final Stage window = (Stage) this.mainPageContent.getScene().getWindow();
        window.setScene(wheelViewScene);
        window.show();
        Logger.info("PAGE CHANGING: Changed to home page from game over page");
    }

    /**
     * This method is used to play another game.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void replay() throws IOException {
        final Parent gameParent = FXMLLoader.load(ClassLoader.getSystemResource("layouts/gamePage.fxml"));
        final Scene gameScene = new Scene(gameParent);
        final Stage window = (Stage) this.mainPageContent.getScene().getWindow();
        window.setScene(gameScene);
        window.show();
        Logger.info("PAGE CHANGING: Changed to new game page from game over page");
    }

    /**
     * This method is used to set the Player Score when game page call this class.
     * @param score This is the score performed by the player.
     * @param money This is the amount of money earned by the player.
     * @throws IOException Throws IOException if any error occurred.
     */
    public void setPlayerScore(final int score, final int money) throws IOException {

        if (listPlayers.getCurrentPlayer().isPresent()) {
            final Player currentPlayer = this.listPlayers.getCurrentPlayer().get();
            currentPlayer.setNewScore(score);
            currentPlayer.addMoney(money);
            listPlayers.update();
            lblPunteggio.setText(String.valueOf(currentPlayer.getLastScore()));
            lblBestScore.setText(String.valueOf(currentPlayer.getBestScore()));
            lblMonete.setText(String.valueOf(currentPlayer.getCurrentMoney()));
            lblMonetineAggiuntive.setText("+" + money);
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * This method is called to get to the FortuneWheelPage.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void newColor() throws IOException {
        if (listPlayers.getCurrentPlayer().isEmpty()) {
            throw new IllegalStateException();
        }
        if (listPlayers.getCurrentPlayer().get().getCurrentMoney() < 1) {
            lblStateMessage.setText("You need at least 1 coin");
            return;
        }

        final Pane pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/fortuneWheelPage.fxml"));
        this.mainPageContent.getChildren().setAll(pane);
        Logger.info("PAGE CHANGING: Changed to fortune wheel page from game over page");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        final RotateTransition rotateTransition = new RotateTransition(Duration.seconds(DURATION), this.btnAddBall);
        rotateTransition.setFromAngle(-ANGLE);
        rotateTransition.setToAngle(ANGLE);
        rotateTransition.setAutoReverse(true);
        rotateTransition.setCycleCount(PathTransition.INDEFINITE);

        if (rotateTransition.getStatus() == Animation.Status.RUNNING) {
            rotateTransition.pause();
        } else {
            rotateTransition.play();
        }

        btnAddBall.setOnMouseEntered(ImageEffects.imgMouseOver());
        btnAddBall.setOnMouseExited(ImageEffects.imgMouseExit());
        restartButtonnn.setOnMouseEntered(ImageEffects.imgMouseOver());
        restartButtonnn.setOnMouseExited(ImageEffects.imgMouseExit());
        stats.setOnMouseEntered(ImageEffects.imgMouseOver());
        stats.setOnMouseExited(ImageEffects.imgMouseExit());
        btnHome.setOnMouseEntered(ImageEffects.imgMouseOver());
        btnHome.setOnMouseExited(ImageEffects.imgMouseExit());
        try {
            listPlayers = PlayersCollectionImpl.getCollectionFromDisk();
            if (listPlayers.getCurrentPlayer().isEmpty()) {
                throw new IllegalStateException();
            } else {
                final Player p = listPlayers.getCurrentPlayer().get();
                lblPunteggio.setText(String.valueOf(p.getLastScore()));
                lblBestScore.setText(String.valueOf(p.getBestScore()));
                lblMonete.setText(String.valueOf(p.getCurrentMoney()));
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
