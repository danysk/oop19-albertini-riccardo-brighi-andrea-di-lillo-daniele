package controller.viewcontroller;

import controller.gamecontroller.GameController;
import controller.gamecontroller.GameControllerImpl;
import controller.maincontroller.MainController;
import controller.maincontroller.MainControllerImpl;
import controller.mouseactions.MouseMovement;
import element.Point2D;
import element.Point2DImpl;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import javafx.fxml.FXML;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javafx.scene.control.Label;

import java.util.Set;
import java.util.stream.Collectors;

import javafx.util.Duration;
import org.json.simple.parser.ParseException;
import view.ball.BallView;
import view.ball.BallViewFactoryCircle;
import view.block.BlockViewImpl;
import view.bonus.BonusView;
import view.bonus.BonusViewImpl;

/**
 * The Controller related to the gamePage.fxml GUI.
 */
public final class ViewGameControllerImpl implements ViewGameController {

    @FXML
    private Pane mainPane;

    @FXML
    private BorderPane gameBorderPane;

    @FXML
    private BorderPane blockPane;

    @FXML
    private Label lblPoint;

    @FXML
    private Label lblTopScore;

    @FXML
    private Button btnEasterEggRight;

    @FXML
    private Button btnEasterEggTop;

    @FXML
    private Button btnEasterEggLeft;

    @FXML
    private Button btnEasterEggBottom;

    /*Pause Controller*/
    private PauseController pauseController;

    /*Help Controller*/
    private HelpViewController helpController;

    /*Main Controller*/
    private MainController mainController;

    /*Game Controller*/
    private GameController gameController;

    private static double blockPaneWidth = 350;
    private int playerBestScore;
    private int easterEgg;
    private List<Shape> myBlock = new ArrayList<>();

    private static final double BLOCKPANE_HEIGHT = 600;
    private static final double FRAME_DURATION = 1.8;
    private static final double DEFAULT_X_STEP = 50;
    private static final double DEFAULT_Y_STEP = 30;
    private static final double HEIGHT_IMG = 225;
    private static final double WIDTH_IMG = 400;
    private static final double X_IMG = -17.5;
    private static final double Y_IMG = 100;

    public void initialize() throws IOException, ParseException {

        this.playerBestScore = 0;
        this.easterEgg = 0;
        this.blockPane.setPrefWidth(this.blockPaneWidth);
        this.blockPane.setPrefHeight(BLOCKPANE_HEIGHT);

        this.addPausePage();
        this.addHelpPage();

        /* Send main controller to Pause Controller*/
        this.mainController = new MainControllerImpl(this);
        this.gameController = new GameControllerImpl(this.mainController, this);

        final Set<Point2D> firstPosition = new HashSet<>();
        firstPosition.add(this.mainController.getBallsStartPosition());
        this.drawBalls(firstPosition);

        this.playerBestScore = this.mainController.getPlayerBestScore();
        this.lblTopScore.setText(Integer.toString(this.playerBestScore));

        this.myBlock = this.gameController.newBlockLine(1);
        this.setMap();

        new MouseMovement(this.gameBorderPane, this.mainController).addAllEvent();
    }

    @Override
    public Point2D getPosition(final Shape shape) {
        if (shape instanceof BlockViewImpl) {
            return new Point2DImpl(((BlockViewImpl) shape).getX(), ((BlockViewImpl) shape).getY());
        } else {
            return new Point2DImpl(((BonusView) shape).getPosition().getX(), ((BonusView) shape).getPosition().getY());
        }
    }

    private void addPausePage() throws IOException {
        /* Aggiungiamo pagina Pause */
        final FXMLLoader loaderPause = new FXMLLoader(ClassLoader.getSystemResource("layouts/pause.fxml"));
        final Pane panePause = loaderPause.load();
        this.pauseController = loaderPause.getController();
        this.pauseController.init(() -> this.mainController.restartGame());
        this.mainPane.getChildren().add(panePause);
    }

    private void addHelpPage() throws IOException {
        /* Aggiungiamo pagina Help */
        final FXMLLoader loaderHelp = new FXMLLoader(ClassLoader.getSystemResource("layouts/helpPage.fxml"));
        final Pane paneHelp = loaderHelp.load();
        this.helpController = loaderHelp.getController();
        this.helpController.init(false, () -> this.mainController.restartGame());
        this.mainPane.getChildren().add(paneHelp);
    }

    private void addLosePage() throws IOException {
        final FXMLLoader loaderEndGame = new FXMLLoader(ClassLoader.getSystemResource("layouts/endGamePage.fxml"));
        final Parent root = loaderEndGame.load();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(this.mainPane.getScene().getHeight());
        //Add second scene. Now both first and second scene is present
        this.mainPane.getChildren().add(root);

        /*FinalPage Controller*/
        final FinalPageController finalPageController = loaderEndGame.getController();
        finalPageController.setPlayerScore(Integer.parseInt(this.lblPoint.getText()), this.mainController.getEarnedMoney());

        //Create new TimeLine animation
        final Timeline timeline = new Timeline();
        //Animate Y property
        final KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        final KeyFrame kf = new KeyFrame(Duration.seconds(FRAME_DURATION), kv);
        timeline.getKeyFrames().add(kf);
        timeline.play();
    }

    @Override
    public double getBlockPaneWidth() {
        return this.blockPaneWidth;
    }

    @Override
    public List<Shape> getMyBlock() {
        return this.myBlock;
    }

    @Override
    public void drawBalls(final Set<Point2D> point2DSet) {
        Platform.runLater(() -> {
            this.blockPane.getChildren().removeIf(i -> i instanceof BallView);
            this.blockPane.getChildren()
                    .addAll(point2DSet.stream()
                            .map(i -> new BallViewFactoryCircle().standardBalls(i, this.mainController.getRadius(), this.mainController.getColor()))
                            .collect(Collectors.toSet()));
        });
    }

    private void setMap() {
        this.deleteAllBlocks();
        this.myBlock.forEach(i -> this.blockPane.getChildren().add(i));
        this.myBlock.stream()
                .filter(i -> i instanceof BlockViewImpl)
                .forEach(i -> this.blockPane.getChildren().add(((BlockViewImpl) i).getText()));
    }

    private void deleteAllBlocks() {
        this.myBlock.forEach(i -> this.blockPane.getChildren().remove(i));
        this.myBlock.stream()
                .filter(i -> i instanceof BlockViewImpl)
                .forEach(i -> this.blockPane.getChildren().remove(((BlockViewImpl) i).getText()));
    }

    private void updateBestScore() {
        if (Integer.parseInt(this.lblPoint.getText()) > this.playerBestScore) {
            this.lblTopScore.setText(this.lblPoint.getText());
            this.playerBestScore = Integer.parseInt(this.lblPoint.getText());
        }
    }

    private void addNewBlockLine(final List<Shape> list) {
        Platform.runLater(() -> {
            for (final Shape block : this.myBlock) {
                double y = this.getPosition(block).getY();
                y += DEFAULT_X_STEP;
                if (block instanceof BlockViewImpl) {
                    final BlockViewImpl selectedBlock = (BlockViewImpl) block;
                    selectedBlock.updateBrightnessColor(false);
                    selectedBlock.setY(y);
                    selectedBlock.getText().setY(y + DEFAULT_Y_STEP);
                } else {
                    final BonusView selectedBonus = (BonusView) block;
                    selectedBonus.setPosition(new Point2DImpl(selectedBonus.getPosition().getX(), y));
                }
            }

            this.myBlock.addAll(list);
            this.setMap();
            this.incrementPointLabel();
            this.removeBonus();

            final int checkBonus = (int) this.myBlock.stream()
                    .filter(i -> i instanceof BonusViewImpl).count();

            if (checkBonus < 3) {
                this.gameController.setBonusProbability(2);
            }

            this.gameController.setBonusBlock();
            if (this.gameController.checkLose()) {
                this.mainController.gameOver();
                try {
                    this.addLosePage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                this.updateBestScore();
            }
        });
    }

    @Override
    public void newLine() {
        this.addNewBlockLine(this.gameController.newBlockLine(Integer.parseInt(this.lblPoint.getText()) + 1));
    }

    @Override
    public void columnHit(final double index) {
        Platform.runLater(() -> this.myBlock.stream()
                .filter(i -> i instanceof BlockViewImpl)
                .filter(i -> ((BlockViewImpl) i).getX() == index - (this.gameController.getDifferenceBonusNormal() - i.getStrokeWidth() / 2))
                .forEach(i -> this.normalBlockHit(i, 1)));
    }

    @Override
    public void rowHit(final double index) {
        Platform.runLater(() -> this.myBlock.stream()
                .filter(i -> i instanceof BlockViewImpl)
                .filter(i -> ((BlockViewImpl) i).getY() == index - (this.gameController.getDifferenceBonusNormal() - i.getStrokeWidth() / 2))
                .forEach(i -> this.normalBlockHit(i, 1)));
    }

    @Override
    public void blockHit(final List<Shape> blockList, final int amount) {
        for (final Shape block : blockList) {
            if (block instanceof BlockViewImpl) {
                this.normalBlockHit(block, amount);
            } else {
                this.bonusBlockHit(block);
            }
        }
    }

    private void normalBlockHit(final Shape block, final int amount) {
        Platform.runLater(() -> {
            final BlockViewImpl blockHitted = (BlockViewImpl) block;
            blockHitted.isHitted(amount);
            if (blockHitted.deletedBlock()) {
                this.blockPane.getChildren().remove(blockHitted);
                this.blockPane.getChildren().remove(blockHitted.getText());
                this.myBlock.remove(block);
            }
        });
    }

    private void bonusBlockHit(final Shape bonusBlock) {
        Platform.runLater(() -> {
            final BonusViewImpl bonusHitted = (BonusViewImpl) bonusBlock;
            final boolean bt = bonusHitted.getLogicBonus().hit();
            if (bt) {
                this.blockPane.getChildren().remove(bonusBlock);
                this.myBlock.remove(bonusBlock);
            }
        });
    }

    private void incrementPointLabel() {
        this.lblPoint.setText(
                Integer.toString(Integer.parseInt(this.lblPoint.getText()) + 1));
    }

    private void removeBonus() {
        final List<Shape> tmpBonus = new ArrayList<>();
        final int rowDelete = 5;
        this.myBlock.stream()
                .filter(i -> i instanceof BonusView)
                .filter(i -> ((BonusView) i).getPosition().getY() == (rowDelete * DEFAULT_X_STEP) + this.gameController.getHeaderY() + this.gameController.getDifferenceBonusNormal() || ((BonusView) i).getLogicBonus().isHitted())
                .forEach(i -> {
                    this.blockPane.getChildren().remove(i);
                    tmpBonus.add(i);
                });

        tmpBonus.forEach(i -> this.myBlock.remove(i));
    }

    public void clickPause() {
        this.mainController.pauseGame();
        this.pauseController.apriPause();
    }

    public void clickHelp() {
        this.mainController.pauseGame();
        this.helpController.openHelp();
    }


    /*EASTER EGG*/

    public void clickRight() {
        this.btnEasterEggRight.setDisable(true);
        this.easterEgg++;
        if (this.easterEgg == 4) {
            this.runEasterEgg();
        }
    }

    public void clickLeft() {
        this.btnEasterEggLeft.setDisable(true);
        this.easterEgg++;
        if (this.easterEgg == 4) {
            this.runEasterEgg();
        }
    }

    public void clickTop() {
        this.btnEasterEggTop.setDisable(true);
        this.easterEgg++;
        if (this.easterEgg == 4) {
            this.runEasterEgg();
        }
    }

    public void clickBottom() {
        this.btnEasterEggBottom.setDisable(true);
        this.easterEgg++;
        if (this.easterEgg == 4) {
            this.runEasterEgg();
        }
    }

    private void runEasterEgg() {
        final ImageView img = new ImageView();
        img.setImage(new Image(String.valueOf(ClassLoader.getSystemResource("images/EasterEgg.png"))));
        img.setFitHeight(HEIGHT_IMG);
        img.setFitWidth(WIDTH_IMG);
        img.setX(X_IMG);
        img.setY(Y_IMG);
        this.gameBorderPane.getChildren().add(img);
        for (int i = 0; i < 100; i++) {
            this.gameController.addBall(); this.incrementPointLabel();
        }
    }

}
