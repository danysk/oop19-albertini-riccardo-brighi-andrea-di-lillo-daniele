package controller.viewcontroller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import utility.BallColor;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Controller that show the player info.
 */
public final class PlayerView implements Initializable {


    @FXML
    private Label lblTopScore;
    @FXML
    private Label lblName;
    @FXML
    private Label lblCoin;
    @FXML
    private Pane rootPane;
    @FXML
    private ChoiceBox<BallColor> boxColor;
    @FXML
    private ChoiceBox<Integer> boxRadius;
    @FXML
    private Circle circleBall;
    @FXML
    private Label lblDamage;

    private PlayersCollection collection;
    private Player player;

    private void updateLabel(final Label label, final String value) {
        label.setText(label.getText() + value);
    }


    /**
     * Update the value of the view elements.
     *
     * @param location The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resources The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        try {
            collection = PlayersCollectionImpl.getCollectionFromDisk();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        if (collection.getCurrentPlayer().isEmpty()) {
            try {
                final Pane mainPane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/playerInsertionPage.fxml"));
                rootPane.getChildren().setAll(mainPane);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        player = collection.getCurrentPlayer().get();


        this.updateLabel(lblName, player.getName());
        this.updateLabel(lblTopScore, String.valueOf(player.getBestScore()));
        this.updateLabel(lblCoin, String.valueOf(player.getCurrentMoney()));
        this.updateLabel(lblDamage, String.valueOf(player.getCurrentBallDamage()));

        boxColor.getItems().addAll(player.getColors());
        boxColor.getSelectionModel().select(player.getCurrentColor());
        boxRadius.getItems().addAll(player.getUnlockedRadius());
        boxRadius.getSelectionModel().select(player.getCurrentBallRadius());
        this.onChange();

    }

    /**
     * Update the value of radius and color of the view ball.
     */
    @FXML
    private void onChange() {
        if (Objects.nonNull(boxRadius.getValue())) {
            final int radius = boxRadius.getValue() * 4;
            circleBall.setRadius(radius);
        }
        if (Objects.nonNull(boxColor.getValue())) {
            final Color color = boxColor.getValue().getColor();
            circleBall.setFill(color);
        }
    }

    /**
     * Go back to home page.
     * @throws IOException if there is a error in the loading of the page
     */
    @FXML
    private void backHome() throws IOException {
        final Pane mainPane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        rootPane.getChildren().setAll(mainPane);
    }

    /**
     * Change the value in the Player.
     */
    @FXML
    private void onConfirmClick() {
        final int radius = boxRadius.getValue();
        final BallColor ballColor = boxColor.getValue();
        player.setCurrentColor(ballColor);
        player.modifyCurrentBallRadius(radius);
        try {
            collection.update();
            this.backHome();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Logout the player.
     */
    @FXML
    private void onClickLogout() {
        try {
            collection.logout();
            final Pane mainPane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/playerInsertionPage.fxml"));
            rootPane.getChildren().setAll(mainPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
