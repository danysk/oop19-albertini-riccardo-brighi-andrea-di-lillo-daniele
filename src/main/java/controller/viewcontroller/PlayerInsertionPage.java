package controller.viewcontroller;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import utility.ImageEffects;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class defines a controller for the playerInsertionPage.fxml, managing its events.
 */
public final class PlayerInsertionPage implements Initializable {

    @FXML
    private ImageView imgPlay;

    @FXML
    private Label lblControl;

    @FXML
    private TextField txtTextBox;

    @FXML
    private Pane mainPageContent;

    @FXML
    private Label lblCurrentPlayer;

    /**
     * This method is used to select the player written and login.
     * @throws IOException Throws IOException if any error occurred.
     * @throws ParseException ParseException Throws IOEsxception if any error occurred.
     */
    @FXML
    public void btnSelectOnClick() throws IOException, ParseException {
        if (txtTextBox.getText().isEmpty()) {
            return;
        }
        final String rawName = txtTextBox.getText();
        final String name = rawName.toUpperCase();
        int present = 0;
        final PlayersCollection leaderboard = PlayersCollectionImpl.getCollectionFromDisk();

        for (final Player p : leaderboard.getPlayers().get()) {
            if (p.getName().equals(name)) {
                present = 1;
            }
        }
        if (present == 1) {
            leaderboard.setCurrentPlayer(name);
            lblControl.setText("Welcome  " + name);
        } else {
            leaderboard.addPlayer(name);
            leaderboard.setCurrentPlayer(name);
            lblControl.setText("Created new profile");
        }
        lblCurrentPlayer.setText(name);
        leaderboard.update();
    }

    /**
     * This method is called to get back on main page.
     * @throws IOException Throws IOException if any error occurred.
     */
    @FXML
    public void btnGetBackOnClick() throws IOException {
        final Pane pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        mainPageContent.getChildren().setAll(pane);
    }

    /**
     * This method is called to play game.
     * @throws IOException Throws IOException if any error occurred.
     * @throws ParseException Throws ParseException if any error occurred.
     */
    @FXML
    public void imgPlayOnClick() throws IOException, ParseException {
        if (PlayersCollectionImpl.getCollectionFromDisk().getCurrentPlayer().isEmpty()) {
            lblControl.setText("Not logged");
            return;
        }
        final Parent startGameParent = FXMLLoader.load(ClassLoader.getSystemResource("layouts/gamePage.fxml"));
        final Scene gameScene = new Scene(startGameParent);
        final Stage window = (Stage) this.mainPageContent.getScene().getWindow();
        window.setScene(gameScene);
        window.show();
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        PlayersCollection leaderboard;
        try {
            leaderboard = PlayersCollectionImpl.getCollectionFromDisk();
            if (leaderboard.getCurrentPlayer().isPresent()) {
                lblCurrentPlayer.setText(leaderboard.getCurrentPlayer().get().getName());
            } else {
                lblCurrentPlayer.setText("");
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        lblControl.setText("");
        imgPlay.setOnMouseExited(ImageEffects.imgMouseExit());
        imgPlay.setOnMouseEntered(ImageEffects.imgMouseOver());
    }
}
