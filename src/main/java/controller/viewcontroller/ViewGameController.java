package controller.viewcontroller;

import element.Point2D;
import javafx.scene.shape.Shape;

import java.util.List;
import java.util.Set;

/**
 * Interface for View
 */
public interface ViewGameController {

    /**
     * @return width of BorderPane called BlockPane
     */
    double getBlockPaneWidth();

    /**
     * @return list of block currently in game
     */
    List<Shape> getMyBlock();

    /**
     * @param point2DSet is set of x and y about every balls in game
     */
    void drawBalls(Set<Point2D> point2DSet);

    /**
     * used to create a new line of blocks.
     */
    void newLine();

    /**
     * @param blockList all block hitted (normal and bonus)
     * @param amount amount of hit
     * when ball hit a normal block.
     */
    void blockHit(List<Shape> blockList, int amount);

    /**
     * @param index value of the column hitted
     * hit all blocks of the index column.
     */
    void columnHit(double index);

    /**
     * @param index value of the row hitted
     * hit all blocks of the index row.
     */
    void rowHit(double index);

    /**
     * @param shape one block (can be bonus or normal)
     * @return the position of shape
     */
    Point2D getPosition(Shape shape);
}
