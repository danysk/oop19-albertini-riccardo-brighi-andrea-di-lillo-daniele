package controller.ballscontroller.launch;

import controller.ballscontroller.time.TimeConverter;
import controller.ballscontroller.ballsagent.BallsAgent;
import model.ball.Ball;

import java.util.Set;

/**
 * 
 * Interface for the factory for the LaunchController.
 *
 */
public interface LaunchControllerFactory {

    /**
     * @param balls         the set of ball to launch
     * @param ballsAgent    the class of the ball movement
     * @param timeConverter the unit to convert
     * @param timeInterval  the interval between the launch of a ball and the next one
     * @param minAngle      the minimum angle of the launch ( in degrees)
     * @return a Launch based on the class Thread
     */

    LaunchController standardThreadBallLaunch(Set<Ball> balls, BallsAgent ballsAgent, TimeConverter timeConverter, long timeInterval, double minAngle);

    /**
     * @param balls         the set of ball to launch
     * @param ballsAgent    the class of the ball movement
     * @param timeConverter the unit to convert
     * @param timeInterval  the interval between the launch of a ball and the next one
     * @param minAngle      the minimum angle of the launch ( in degrees)
     * @return a Launch based on the class Thread
     */

    LaunchController standardThreadPauseBallLaunch(Set<Ball> balls, BallsAgent ballsAgent, TimeConverter timeConverter, long timeInterval, double minAngle);
}
