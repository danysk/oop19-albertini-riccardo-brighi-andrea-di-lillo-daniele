package controller.ballscontroller.launch;

import controller.ballscontroller.AbstractPauseThread;
import controller.ballscontroller.time.TimeConverter;
import controller.ballscontroller.ballsagent.BallsAgent;
import element.Vector2D;
import model.ball.Ball;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Factory to create a launchController.
 *
 */
public class LaunchControllerFactoryImpl implements LaunchControllerFactory {

    private static final class LaunchControllerThread extends AbstractPauseThread implements LaunchController {
        private Optional<Vector2D> direction = Optional.empty();
        private final double minAngle;
        private final BallsAgent ballsAgent;
        private final Set<Ball> balls;
        private final TimeConverter timeConverter;
        private final long timeInterval;

        private LaunchControllerThread(final Set<Ball> balls, final BallsAgent ballsAgent, final TimeConverter timeConverter, final long timeInterval, final double minAngle) {
            this.minAngle = minAngle;
            this.ballsAgent = ballsAgent;
            this.balls = balls;
            this.timeInterval = timeInterval;
            this.timeConverter = timeConverter;
        }


        @Override
        public boolean isValidVector(final Vector2D vector2D) {
            final double maxAngle = (180 - Math.abs(minAngle)) * Math.signum(minAngle);
            return vector2D.getDegreesAngle() > minAngle && vector2D.getDegreesAngle() < maxAngle;
        }

        @Override
        public void setVector(final Vector2D direction) {
            if (Objects.isNull(direction) || direction.isNullVector() || !this.isValidVector(direction)) {
                throw new IllegalArgumentException();
            }
            this.direction = Optional.of(direction);
        }

        @Override
        public void loop() {
            for (final Ball ball : balls) {
                if (ball.isStationary()) {
                    ball.setDirection(this.getDirection().orElseThrow(IllegalStateException::new));
                    ball.ableToCollide();
                    ballsAgent.move(ball);
                    try {
                        Thread.sleep((long) (timeConverter.getTimeToMillisecondFromSelected(2 * ball.getRadius() / ball.getSpeed()) + timeInterval));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected boolean conditionOver() {
            return true;
        }

        /**
         * @return the actual vector of the launch if presents or else an optional empty
         */
        protected Optional<Vector2D> getDirection() {
            return direction;
        }

        @Override
        public void launch() {
            this.start();
        }

        /**
         * this method start the method and throw IllegalStateException if the direction is empty.
         */
        @Override
        public void start() {
            if (this.direction.isEmpty()) {
                throw new IllegalStateException();
            }
            super.start();
        }

        @Override
        protected void over() {
            this.direction = Optional.empty();
            super.over();
        }
    }


    @Override
    public final LaunchController standardThreadBallLaunch(final Set<Ball> balls, final BallsAgent ballsAgent, final TimeConverter timeConverter, final long timeInterval, final double minAngle) {
        final LaunchController launchController = standardThreadPauseBallLaunch(balls, ballsAgent, timeConverter, timeInterval, minAngle);
        return new LaunchControllerNoPause(launchController);
    }

    @Override
    public final LaunchController standardThreadPauseBallLaunch(final Set<Ball> balls, final BallsAgent ballsAgent, final TimeConverter timeConverter, final long timeInterval, final double minAngle) {
        if (!this.isAngleValid(minAngle) || Objects.isNull(balls) || Objects.isNull(ballsAgent)) {
            throw new IllegalArgumentException();
        }
        return new LaunchControllerThread(balls, ballsAgent, timeConverter, timeInterval, minAngle);

    }

    private boolean isAngleValid(final double angle) {
        final double rectAngle = 90;
        final double plateAngle = 180;
        return (0 <= angle && angle <= rectAngle) || (-plateAngle <= angle && angle <= -rectAngle);
    }
}
