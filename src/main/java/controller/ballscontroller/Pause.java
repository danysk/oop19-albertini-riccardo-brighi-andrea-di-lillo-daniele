package controller.ballscontroller;

/**
 * Interface for pause something.
 *
 */
public interface Pause {

    /**
     * @throws UnsupportedOperationException if the class doesn't support pause
     *                                       <p>
     *                                       restart it
     *                                       </p>
     */

    void restart();

    /**
     * @throws UnsupportedOperationException if the class doesn't support pause
     *                                       <p>
     *                                       pause it
     *                                       </p>
     */

    void pause();

    /**
     * end the actions.
     */
    void endGame();

    /**
     * @return true if it's in pause else false
     */

    boolean isPause();

    /**
     * @return true if it's over else false
     */

    boolean isOver();

    /**
     * @return true if the execution is end else false
     */
    boolean isEndGame();
}
