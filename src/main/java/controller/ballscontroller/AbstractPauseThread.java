package controller.ballscontroller;

/**
 * Abstract class that use Thread and can pause it.
 *
 */
public abstract class AbstractPauseThread extends Thread implements Pause {

    private volatile boolean isPause;
    private volatile boolean isOver = true;
    private volatile boolean isEndGame;

    /**
     * run, thread method.
     */
    @Override
    public void run() {
        while (!this.isEndGame) {
            this.pauseThread();
            this.loop();
            if (this.conditionOver()) {
                this.over();
            }
        }
    }

    /**
     * the method to implement that is looped until the conditionOver if true and
     * restart when is call method start.
     */
    protected abstract void loop();

    /**
     * 
     * @return true if the thread is over but can be restart
     */
    protected abstract boolean conditionOver();

    /**
     * the method that start the thread, if the thread is already alive restart.
     */
    @Override
    public synchronized void start() {
        this.isOver = false;
        if (!this.isAlive()) {
            super.start();
        } else {
            this.notifyAll();
        }
    }

    /**
     * restart the thread.
     */
    @Override
    public synchronized void restart() {
        if (!this.isOver) {
            this.isPause = false;
            this.notifyAll();
        }
    }

    /**
     * pause the current thread if the variable pause is true (use inside the run or
     * loop method).
     */
    protected synchronized void pauseThread() {
        if (this.isPause) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * pause the thread.
     */
    @Override
    public synchronized void pause() {
        if (!this.isOver) {
            this.isPause = true;
            this.interrupt();
        }
    }

    /**
     * @return true if the thread is pause
     */
    @Override
    public synchronized boolean isPause() {
        return this.isPause;
    }

    /**
     * @return true if the thread is over, but can be run again
     */
    @Override
    public synchronized boolean isOver() {
        return this.isOver;
    }

    /**
     * @return true if the thread is end
     */
    @Override
    public synchronized boolean isEndGame() {
        return this.isEndGame;
    }

    /**
     * set the value for the end (not definitive) for the thread, put it in pause
     * until start.
     */
    protected synchronized void over() {
        this.isOver = true;
        while (this.isOver) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * set the value for the define end of the thread.
     */
    @Override
    public synchronized void endGame() {
        this.isEndGame = true;
        this.interrupt();
        this.notifyAll();
    }
}
