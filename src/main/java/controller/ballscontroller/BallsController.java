package controller.ballscontroller;

import element.Point2D;

import element.Vector2D;
import model.ball.Ball;

import java.util.Set;

/**
 * 
 * Interface that represent the balls controller.
 *
 */
public interface BallsController extends Pause {

    /**
     * @param direction the direction selected
     * @return true direction is valid and the balls are stationary else false
     */

    boolean isValidLaunch(Vector2D direction);

    /**
     * @param direction the direction selected
     * @throws IllegalStateException if the balls are in movement
     */

    void launchBalls(Vector2D direction);

    /**
     * add a ball to the other.
     */

    void addBall();

    /**
     * @return the set of the balls
     */

    Set<Ball> getBalls();

    /**
     * @return true if all the balls have direction equal to the vector arithmetic
     *         null else false
     */

    boolean areBallsStationary();

    /**
     *
     * @return the point of the start position of the balls
     * @throws IllegalStateException if the balls aren't stationary
     */
    Point2D getStartPosition();
}
