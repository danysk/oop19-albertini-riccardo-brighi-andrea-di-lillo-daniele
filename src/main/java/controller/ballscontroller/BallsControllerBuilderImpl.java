package controller.ballscontroller;

import controller.ballscontroller.time.Time;
import controller.ballscontroller.time.TimeConverter;
import controller.ballscontroller.ballsagent.BallsAgent;
import controller.ballscontroller.ballsagent.BallsAgentFactory;
import controller.ballscontroller.ballsagent.MultiThreadBallsAgentFactory;
import controller.ballscontroller.ballsagent.SingleThreadBallsAgentFactory;
import controller.ballscontroller.launch.LaunchController;
import controller.ballscontroller.launch.LaunchControllerFactory;
import controller.ballscontroller.launch.LaunchControllerFactoryImpl;
import element.Point2D;
import element.Vector2D;
import model.ball.Ball;
import model.ball.BallBuilder;
import utility.Counter;
import utility.IntegerCounterFactory;
import utility.RangeFactoryImpl;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The implementation of the BallsControllerBuilder interface.
 *
 */
public class BallsControllerBuilderImpl implements BallsControllerBuilder {

    private static final double STANDARD_ANGLE = 20;

    private Type type;
    private Pause pause;
    private Time time;
    private int initialBalls = 1;
    private long timeInterval = 1;
    private int stepBetweenBalls = 2;
    private BallBuilder ballsBuilder;
    private Point2D startPoint;
    private double angle = STANDARD_ANGLE;
    private TimeConverter timeConverter;
    private Runnable ballsStop;
    private final LaunchControllerFactory launchControllerFactory = new LaunchControllerFactoryImpl();

    /**
     * Check if the @param type is not null and add it.
     */
    @Override
    public BallsControllerBuilder addType(final Type type) {
        if (Objects.isNull(type)) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        return this;
    }

    /**
     * Check if the type is not null and the @param pause is not null and add
     * the @param pause.
     */
    @Override
    public BallsControllerBuilder addPause(final Pause pause) {
        if (Objects.isNull(this.type) || Objects.isNull(pause)) {
            throw new IllegalArgumentException();
        }
        this.pause = pause;
        return this;
    }

    /**
     * Check if the type is not null and the @param time is not null and add
     * the @param time.
     */
    @Override
    public BallsControllerBuilder addTime(final Time time) {
        if (Objects.isNull(this.type) || Objects.isNull(time)) {
            throw new IllegalArgumentException();
        }
        this.time = time;
        return this;
    }

    /**
     * Check if the @param angle angle is not between 0 90 and -90 -180 and ad it.
     */
    @Override
    public BallsControllerBuilder addMinusAngleDegrees(final double angle) {
        final double rectAngle = 90;
        final double plateAngle = 180;
        if (!((0 <= angle && angle <= rectAngle) || (-plateAngle <= angle && angle <= -rectAngle))) {
            throw new IllegalArgumentException();
        }
        this.angle = angle;
        return this;
    }

    /**
     * /** Check if the @param angle is not between 0 and pi/2 and not between -pi/2
     * and -pi.
     */
    @Override
    public BallsControllerBuilder addMinusAngleRadians(final double angle) {
        return this.addMinusAngleDegrees(angle * 180 / Math.PI);
    }

    /**
     * Check if @param timeConverter is not null and add it.
     */
    @Override
    public BallsControllerBuilder addTimeMeasures(final TimeConverter timeConverter) {
        if (Objects.isNull(time)) {
            throw new IllegalArgumentException();
        }
        this.timeConverter = timeConverter;
        return this;
    }

    /**
     * Check if @param initialBalls is grater than 0 and add it.
     */
    @Override
    public BallsControllerBuilder addInitialBalls(final int initialBalls) {
        if (initialBalls <= 0) {
            throw new IllegalArgumentException();
        }
        this.initialBalls = initialBalls;
        return this;
    }

    /**
     * Check if @param ballsBuilder is not null and add it.
     */
    @Override
    public BallsControllerBuilder addBallsBuilder(final BallBuilder ballsBuilder) {
        if (Objects.isNull(ballsBuilder)) {
            throw new IllegalArgumentException();
        }
        this.ballsBuilder = ballsBuilder;
        return this;
    }

    /**
     * Check if @param startPoint is not null and add it.
     */
    @Override
    public BallsControllerBuilder addStartPosition(final Point2D startPoint) {
        if (Objects.isNull(startPoint)) {
            throw new IllegalArgumentException();
        }
        this.startPoint = startPoint;
        return this;
    }

    /**
     * Check if the @param timeInterval is grater than 0 and add it.
     */
    @Override
    public BallsControllerBuilder addTimeInterval(final long timeInterval) {
        if (timeInterval <= 0) {
            throw new IllegalArgumentException();
        }
        this.timeInterval = timeInterval;
        return this;
    }

    /**
     * Check if the @param stepBetweenBalls is grater than 0 and add it.
     */
    @Override
    public BallsControllerBuilder addNumberOfStepBetweenBalls(final int stepBetweenBalls) {
        if (stepBetweenBalls < 0) {
            throw new IllegalArgumentException();
        }
        this.stepBetweenBalls = stepBetweenBalls;
        return this;
    }

    /**
     * Check if @param endLaunch is not null and add it.
     */
    @Override
    public BallsControllerBuilder addEndLaunch(final Runnable endLaunch) {
        if (Objects.isNull(endLaunch)) {
            throw new IllegalArgumentException();
        }
        this.ballsStop = endLaunch;
        return this;
    }

    /**
     * build the BallsController.
     */
    @Override
    public BallsController build() {
        if (Objects.isNull(this.type) || Objects.isNull(this.time) || Objects.isNull(this.pause)
                || Objects.isNull(this.timeConverter)) {
            throw new IllegalStateException();
        }

        BallsAgentFactory ballsAgentFactory;
        switch (this.type) {
        case SINGLE_THREAD:
            ballsAgentFactory = new SingleThreadBallsAgentFactory();
            break;
        case MULTI_THREAD:
            ballsAgentFactory = new MultiThreadBallsAgentFactory();
            break;
        default:
            throw new IllegalStateException("Unexpected value: " + this.type);
        }
        final Set<Ball> balls = new HashSet<>();
        BallsAgent ballsAgent;
        LaunchController launchController;
        switch (this.pause) {
        case PAUSE:
            ballsAgent = ballsAgentFactory.pauseBallsAgent(this.startPoint, this.timeConverter, this.timeInterval,
                    this.time);
            launchController = this.launchControllerFactory.standardThreadPauseBallLaunch(balls, ballsAgent,
                    this.timeConverter, this.timeInterval * this.stepBetweenBalls, angle);
            break;
        case UNPAUSE:
            ballsAgent = ballsAgentFactory.noPauseBallsAgent(this.startPoint, this.timeConverter, this.timeInterval,
                    this.time);
            launchController = this.launchControllerFactory.standardThreadBallLaunch(balls, ballsAgent,
                    this.timeConverter, this.timeInterval * this.stepBetweenBalls, this.angle);
            break;
        default:
            throw new IllegalStateException("Unexpected value: " + this.pause);
        }
        final AtomicBoolean notify = new AtomicBoolean(false);
        final Counter<Integer> ballsToAdd = new IntegerCounterFactory().standardCounter();
        this.ballsBuilder = this.ballsBuilder.addStop(ball -> {
            ballsAgent.setEndPoint(ball.getPosition());
            ballsAgent.groupBall(ball);
            synchronized (notify) {
                if (ballsAgent.areBallsStationary() && ballsAgent.areBallsGrouped() && !notify.get()) {
                    for (final int ignored : new RangeFactoryImpl().standardRange(ballsToAdd.getValue())) {
                        synchronized (balls) {
                            balls.add(ballsBuilder.addStartPosition(ballsAgent.getStartPoint()).build());
                        }
                    }
                    ballsToAdd.reset();
                    ballsStop.run();
                    notify.set(true);
                }
            }
        });

        for (final int ignored : new RangeFactoryImpl().standardRange(this.initialBalls)) {
            balls.add(this.ballsBuilder.addStartPosition(ballsAgent.getStartPoint()).build());
        }
        return new BallsController() {
            @Override
            public boolean isValidLaunch(final Vector2D direction) {
                return launchController.isValidVector(direction) && this.areBallsStationary();
            }

            @Override
            public void launchBalls(final Vector2D direction) {
                if (!this.areBallsStationary() || !this.isValidLaunch(direction)) {
                    throw new IllegalStateException();
                }
                launchController.setVector(direction.getNormalizedVector());
                launchController.launch();
                notify.set(false);
            }

            @Override
            public void addBall() {
                if (this.areBallsStationary()) {
                    synchronized (balls) {
                        balls.add(ballsBuilder.addStartPosition(ballsAgent.getStartPoint()).build());
                    }
                } else {
                    ballsToAdd.increment();
                }
            }

            @Override
            public Set<Ball> getBalls() {
                synchronized (balls) {
                    return Set.copyOf(balls);
                }
            }

            @Override
            public boolean areBallsStationary() {
                return ballsAgent.isOver() || (ballsAgent.areBallsStationary() && ballsAgent.areBallsGrouped());
            }

            @Override
            public Point2D getStartPosition() {
                if (!this.areBallsStationary()) {
                    throw new IllegalStateException();
                }
                return ballsAgent.getStartPoint();
            }

            @Override
            public void pause() {
                ballsAgent.pause();
                launchController.pause();
            }

            @Override
            public void endGame() {
                ballsAgent.endGame();
                launchController.endGame();

            }

            @Override
            public boolean isPause() {
                return ballsAgent.isPause() || launchController.isPause();
            }

            @Override
            public boolean isOver() {
                return ballsAgent.isOver() || launchController.isOver();
            }

            @Override
            public boolean isEndGame() {
                return ballsAgent.isEndGame() || launchController.isEndGame();
            }

            @Override
            public void restart() {
                ballsAgent.restart();
                launchController.restart();
            }
        };
    }
}
