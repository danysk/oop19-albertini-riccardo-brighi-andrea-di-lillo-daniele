package controller.ballscontroller.ballsagent;

import controller.ballscontroller.AbstractMoveThread;
import controller.ballscontroller.Pause;
import controller.ballscontroller.time.Time;
import controller.ballscontroller.time.TimeConverter;
import element.Point2D;
import model.ball.Ball;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Factory for the BallsAgents, define joint operations.
 *
 */
public abstract class AbstractBallsAgentFactory implements BallsAgentFactory {

    /**
     * 
     * Interface used to move the balls.
     */
    protected interface BallsAgentMovement extends Pause {

        /**
         * @param ball the ball to move
         */
        void move(Ball ball);

        /**
         * @param ball the ball selected
         * @return the Thread that move the ball
         */

        AbstractMoveThread getBallThread(Ball ball);

        /**
         * @return the set of balls
         */
        Set<Ball> getBalls();

    }

    /**
     * create a BallsAgent that not support pause.
     */
    @Override
    public BallsAgent noPauseBallsAgent(final Point2D startPoint, final TimeConverter timeConverter,
            final long timeInterval, final Time time) {
        return new BallsAgentNoPause(this.pauseBallsAgent(startPoint, timeConverter, timeInterval, time));
    }

    /**
     * create a BallsAgent that support pause.
     */
    @Override
    public BallsAgent pauseBallsAgent(final Point2D startPoint, final TimeConverter timeConverter,
            final long timeInterval, final Time time) {
        return new BallsAgent() {

            private Point2D startPosition = startPoint;
            private Optional<Point2D> endPoint = Optional.empty();
            private final BallsAgentMovement ballsAgentMovement = ballsAgentMovement(timeConverter, timeInterval, time);

            @Override

            public void move(final Ball ball) {
                this.ballsAgentMovement.move(ball);
            }

            @Override
            public synchronized void setEndPoint(final Point2D endPoint) {
                if (this.endPoint.isEmpty()) {
                    this.endPoint = Optional.of(endPoint);
                }
            }

            @Override
            public Point2D getStartPoint() {
                return this.startPosition;
            }

            @Override
            public synchronized Optional<Point2D> getEndPoint() {
                return this.endPoint;
            }

            @Override
            public void groupBall(final Ball ball) {
                synchronized (this) {
                    if (this.getEndPoint().isEmpty()) {
                        throw new IllegalStateException();
                    }
                }
                if (Objects.isNull(ball)) {
                    throw new IllegalArgumentException();
                }
                if (!ball.getPosition().equals(this.getEndPoint().get())) {
                    ball.setDirection(
                            this.getEndPoint().orElseThrow(IllegalStateException::new).subtraction(ball.getPosition()));
                    ball.setDestination(this.getEndPoint().get());
                    ball.notAbleToCollide();
                    final AbstractMoveThread thread = this.ballsAgentMovement.getBallThread(ball);
                    if (thread.isOver()) {
                        thread.start();
                    }
                }
                if (this.areBallsStationary()) {
                    synchronized (this) {
                        this.startPosition = this.getEndPoint().get();
                        this.endPoint = Optional.empty();
                    }
                }
            }

            @Override
            public boolean areBallsStationary() {
                synchronized (this.ballsAgentMovement) {
                    return this.ballsAgentMovement.getBalls().isEmpty()
                            || this.ballsAgentMovement.getBalls().stream().allMatch(Ball::isStationary);
                }
            }

            @Override
            public boolean areBallsGrouped() {
                synchronized (this.ballsAgentMovement) {
                    return this.ballsAgentMovement.getBalls().isEmpty() || this.ballsAgentMovement.getBalls().stream()
                            .map(Ball::getPosition).distinct().count() == 1;
                }
            }

            @Override
            public void restart() {
                this.ballsAgentMovement.restart();
            }

            @Override
            public void pause() {
                this.ballsAgentMovement.pause();
            }

            @Override
            public void endGame() {
                this.ballsAgentMovement.endGame();
            }

            @Override
            public boolean isPause() {
                return this.ballsAgentMovement.isPause();
            }

            @Override
            public boolean isOver() {
                return this.ballsAgentMovement.isOver();
            }

            @Override
            public boolean isEndGame() {
                return this.ballsAgentMovement.isEndGame();
            }
        };
    }

    /**
     * @param timeConverter the unit to converter
     * @param timeInterval  the interval between the ball
     * @param calculateTime the function to calculate the time
     * @return a BallsAgentInternal
     */
    protected abstract BallsAgentMovement ballsAgentMovement(TimeConverter timeConverter, long timeInterval,
            Time calculateTime);
}
