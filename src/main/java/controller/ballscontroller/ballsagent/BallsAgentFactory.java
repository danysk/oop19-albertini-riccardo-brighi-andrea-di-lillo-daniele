package controller.ballscontroller.ballsagent;

import controller.ballscontroller.time.Time;

import controller.ballscontroller.time.TimeConverter;
import element.Point2D;

/**
 * 
 * Interface to create the BallsAgent.
 */

public interface BallsAgentFactory {

    /**
     * @param startPoint    the start point
     * @param timeConverter the unit to convert
     * @param timeInterval  the time to wait for a movement
     * @param time          the time of time interval calculation
     * @return a BallAgent
     */

    BallsAgent noPauseBallsAgent(Point2D startPoint, TimeConverter timeConverter, long timeInterval, Time time);

    /**
     * @param startPoint    the start point
     * @param timeConverter the unit to convert
     * @param timeInterval  the time to wait for a movement
     * @param time          the time of time interval calculation
     * @return a BallAgent with pause
     */

    BallsAgent pauseBallsAgent(Point2D startPoint, TimeConverter timeConverter, long timeInterval, Time time);

}
