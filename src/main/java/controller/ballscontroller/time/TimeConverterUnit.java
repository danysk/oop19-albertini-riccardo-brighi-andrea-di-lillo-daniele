package controller.ballscontroller.time;

/**
 * 
 * Possible Implementation of TimeConverter.
 *
 */
public enum TimeConverterUnit implements TimeConverter {
    /**
     * convert millisecond in millisecond.
     */
    MILLISECOND(1.0),
    /**
     * convert millisecond in second.
     */
    SECOND(1000.0);

    private final double scale;

    TimeConverterUnit(final double scale) {
        this.scale = scale;
    }

    @Override
    public double getTimeFromMillisecondToSelected(final double millisecond) {
        return millisecond / this.scale;
    }

    @Override
    public double getTimeToMillisecondFromSelected(final double time) {
        return time * this.scale;
    }
}
