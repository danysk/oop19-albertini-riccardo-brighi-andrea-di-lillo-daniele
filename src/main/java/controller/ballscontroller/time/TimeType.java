package controller.ballscontroller.time;

import java.util.function.BiFunction;

/**
 * Possible implementation of Time.
 *
 */
public enum TimeType implements Time {
    /**
     * use the elapsed time.
     */
    EFFECTIVE_TIME((final Long interval, final Long elapsedTime) -> Double.valueOf(elapsedTime)),
    /**
     * use the interval.
     */
    ABSTRACT_TIME((final Long interval, final Long elapsedTime) -> Double.valueOf(interval));

    private final BiFunction<Long, Long, Double> elapsedTime;

    TimeType(final BiFunction<Long, Long, Double> elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    @Override
    public double getElapsedTime(final long interval, final long elapsedTime) {
        return this.elapsedTime.apply(interval, elapsedTime);
    }
}
