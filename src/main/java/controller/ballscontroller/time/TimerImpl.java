package controller.ballscontroller.time;

/**
 * 
 * Class that implements Timer.
 */
public class TimerImpl implements Timer {

    private final Time calculateTime;
    private final TimeConverter timeConverter;
    private final long timeInterval;
    private long lastUpdate;

    /**
     * 
     * @param calculateTime the time to calculate
     * @param timeConverter the conversion of time
     * @param timeInterval the abstract interval of time
     */
    public TimerImpl(final Time calculateTime, final TimeConverter timeConverter, final long timeInterval) {
        this.calculateTime = calculateTime;
        this.timeInterval = timeInterval;
        this.timeConverter = timeConverter;
    }

    /**
     * reset the timer, set last update to current time.
     */
    @Override
    public void reset() {
        this.lastUpdate = System.currentTimeMillis();
    }

    /**
     *
     * @return the time between the current time and the last update
     */
    @Override
    public double getInterval() {
        final long time = System.currentTimeMillis();
        final long interval = time - this.lastUpdate;
        this.lastUpdate = time;
        return this.timeConverter.getTimeFromMillisecondToSelected(this.calculateTime
                .getElapsedTime(this.timeInterval, interval));
    }
}
