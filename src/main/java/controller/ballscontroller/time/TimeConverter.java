package controller.ballscontroller.time;

/**
 * 
 * Interface for the conversion of time.
 *
 */
public interface TimeConverter {

    /**
     * 
     * @param millisecond the time in millisecond
     * @return the time in the selected time measure
     */
    double getTimeFromMillisecondToSelected(double millisecond);

    /**
     * 
     * @param time the time in the selected unit
     * @return the time in millisecond
     */
    double getTimeToMillisecondFromSelected(double time);
}
