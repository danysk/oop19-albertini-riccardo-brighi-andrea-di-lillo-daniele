package controller.ballscontroller;

import controller.ballscontroller.time.Time;
import controller.ballscontroller.time.TimeConverter;
import controller.ballscontroller.time.Timer;
import controller.ballscontroller.time.TimerImpl;

/**
 * An abstract class that is used for the loop and the pause of the thread.
 */
public abstract class AbstractMoveThread extends AbstractPauseThread {
    private final Timer timer;
    private final long timeInterval;

    /**
     * @param calculateTime the real elapsed time
     * @param timeConverter the timeConverted used
     * @param timeInterval  the abstract interval of time
     */
    protected AbstractMoveThread(final Time calculateTime, final TimeConverter timeConverter, final long timeInterval) {
        this.timeInterval = timeInterval;
        this.timer = new TimerImpl(calculateTime, timeConverter, timeInterval);
    }

    /**
     * method executed in loop, until thread is over or endGame.
     */
    @Override
    protected void loop() {
        this.move(this.timer.getInterval());
        try {
            Thread.sleep(this.timeInterval);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param interval the interval between the actual call and the previous
     */
    protected abstract void move(double interval);

    /**
     * start the thread and the timer.
     */
    @Override
    public synchronized void start() {
        super.start();
        this.timer.reset();
    }

    /**
     * restart the thread and the timer.
     */
    @Override
    public synchronized void restart() {
        super.restart();
        this.timer.reset();
    }
}
