package model.serializator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.IOException;
import java.util.Optional;

/**
 * This interface model a Serializator of a X type object. This interface define methods that must be implemented
 * for the specific object serializator.
 * @param <X> X is the generic type.
 */
public interface Serializator<X> {

    /**
     * @param object The object of generic type X to be converted into json object.
     * @return Return the json object that corresponds to the object converted.
     */
    JSONObject convertIntoJSONObject(X object);

    /**
     * @param object The json object that you want to deserialize, converting it into its generic type X.
     * @return Return the X object deserialized from the JSONObject.
     */
    X deserializeJSONObject(JSONObject object);

    /**
     * This method deserialize a generic object from disk into Optional of the same object.
     *
     * @param path     Is the path where is stored the json file.
     * @param fileName Is the name of the stored file.
     * @return Return the object deserialized succesfully from the json file.
     * @throws IOException    Throws an IOException if reading error occurs, like file don't found.
     * @throws ParseException Throws a ParseException if any parsing error occurs while converting json object.
     */
    default Optional<X> deserialize(String path, String fileName) throws IOException, ParseException {
        JSONParser parserDes = new JSONParser();
        Reader reader = null;
        try {
            reader = new FileReader(path + System.getProperty("file.separator") + fileName);
            JSONObject jsonObject = (JSONObject) parserDes.parse(reader);
            reader.close();
            return Optional.of(this.deserializeJSONObject(jsonObject));
        } catch (IOException e) {
            if (e instanceof FileNotFoundException) {
                return Optional.empty();
            }
            throw e;
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method serialize a generic object into a json file saved into disk.
     *
     * @param path     Is the path where is stored the json file.
     * @param object   Is the X object to be serialized into a json file.
     * @param fileName Is the name of the stored file.
     * @throws IOException Throws an Exception if any parsing or writing error occurs.
     */
    default void serialize(String path, X object, String fileName) throws IOException {
        JSONObject jo = convertIntoJSONObject(object);
        FileWriter file = null;
        boolean wrote = false;
        try {
            file = new FileWriter(path + System.getProperty("file.separator") + fileName);
            file.write(jo.toJSONString());
            file.flush();
            file.close();
            wrote = true;
        } catch (IOException e) {
            Logger.error("ERROR! Cannot open serialization file");
            throw e;
        } finally {
            if (file != null && !wrote) {
                try {
                    file.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
