package model.player;

import model.items.UnlockedItems;
import model.items.UnlockedItemsImpl;
import utility.BallColor;
import utility.Counter;
import utility.IntegerCounterFactory;

import java.util.Optional;
import java.util.Set;

/**
 * This class implements PlayerBuilder interface, implementing its methods.
 */
public final class PlayerBuilderImpl implements PlayerBuilder {

    /**
     * Initial radius of the standard ball.
     */
    private static final int INITIAL_RADIUS = 8;
    private int bestScore;
    private String playerName;
    private int initialMoney;
    private int lastScore;
    private Optional<Counter<Integer>> damage;
    private Optional<UnlockedItems<BallColor>> unlockedColors;
    private Optional<UnlockedItems<Integer>> unlockedRadius;

    public PlayerBuilderImpl() {
        bestScore = 0;
        playerName = "";
        initialMoney = 0;
        lastScore = 0;
        damage = Optional.empty();
        unlockedColors = Optional.empty();
        unlockedRadius = Optional.empty();
    }

    @Override
    public PlayerBuilder addNamePlayer(final String name) {
        this.playerName = name.toUpperCase();
        return this;
    }

    @Override
    public PlayerBuilder addBestScore(final int bestScore) {
        this.bestScore = bestScore;
        return this;
    }

    @Override
    public PlayerBuilder addInitialMoney(final int money) {
        this.initialMoney = money;
        return this;
    }

    @Override
    public PlayerBuilder addLastScore(final int lastScore) {
        this.lastScore = lastScore;
        return this;
    }

    @Override
    public PlayerBuilder addUnlockedColors(final BallColor color, final Set<BallColor> unlockedColors) {
        this.unlockedColors = Optional.of(new UnlockedItemsImpl<>(color, unlockedColors));
        return this;
    }

    @Override
    public PlayerBuilder addUnlockedRadius(final Integer radius, final Set<Integer> unlockedRadius) {
        this.unlockedRadius = Optional.of(new UnlockedItemsImpl<>(radius, unlockedRadius));
        return this;
    }

    @Override
    public PlayerBuilder addDamage(final int damage) {
        this.damage = Optional.of(new IntegerCounterFactory().standardCounterFromValue(damage));
        return this;
    }

    @Override
    public Player build() {
        if (this.playerName.isEmpty()) {
            throw new IllegalStateException();
        }

        if (this.unlockedColors.isEmpty()) {
            this.unlockedColors = Optional.of(new UnlockedItemsImpl<>(BallColor.WHITE));
        }
        if (this.unlockedRadius.isEmpty()) {
            this.unlockedRadius = Optional.of(new UnlockedItemsImpl<>(INITIAL_RADIUS));
        }
        if (damage.isEmpty()) {
            this.damage = Optional.of(new IntegerCounterFactory().standardCounterFromValue(1));
        }

        return new Player() {

            @Override
            public Set<BallColor> getColors() {
                return unlockedColors.get().getItems();
            }

            @Override
            public void addNewColor(final BallColor newColor) {
                unlockedColors.get().addItem(newColor);
            }

            @Override
            public BallColor getCurrentColor() {
                return unlockedColors.get().getCurrentItem();
            }

            @Override
            public void setCurrentColor(final BallColor color) {
                unlockedColors.get().setCurrentItem(color);
            }

            @Override
            public Integer getCurrentBallRadius() {
                return unlockedRadius.get().getCurrentItem();
            }

            @Override
            public Integer getCurrentBallDamage() {
                return damage.get().getValue();
            }

            @Override
            public Set<Integer> getUnlockedRadius() {
                return unlockedRadius.get().getItems();
            }

            @Override
            public void addUnlockedRadius(final int radius) {
                unlockedRadius.get().addItem(radius);
            }

            @Override
            public void incrementDamage() {
                damage.get().increment();
            }

            @Override
            public boolean hasEnoughMoney(final int price) {
                return initialMoney >= price;
            }

            @Override
            public int getCurrentMoney() {
                return initialMoney;
            }

            @Override
            public int getBestScore() {
                return bestScore;
            }

            @Override
            public String getName() {
                if (playerName.isEmpty()) {
                    throw new IllegalStateException();
                }
                return playerName;
            }

            @Override
            public void addMoney(final int money) {
                initialMoney += money;
            }

            @Override
            public void useMoney(final int money) {
                initialMoney -= money;
            }

            @Override
            public void modifyCurrentBallRadius(final int radius) {
                unlockedRadius.get().setCurrentItem(radius);
            }

            @Override
            public void setNewScore(final int newScore) {
                if (bestScore < newScore) {
                    bestScore = newScore;
                }
                lastScore = newScore;
            }

            @Override
            public int getLastScore() {
                return lastScore;
            }
        };
    }
}
