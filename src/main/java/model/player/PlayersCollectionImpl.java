package model.player;

import model.serializator.Serializator;
import model.serializator.SerializatorFactoryImpl;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of PlayersCollection interface that manage a players collection
 * and serializing/deserializing it from disk. It implements all function basically used in
 * a list of player, like adding new player, setting up the current player or getting some info about the collection
 */
public final class PlayersCollectionImpl implements PlayersCollection {

    private List<Player> players;
    private String currentPlayer;

    public PlayersCollectionImpl() {
        players = new ArrayList<>();
        currentPlayer = "";
    }

    /**
     * Static Factory to create the PlayersCollection object.
     *
     * @return return the PlayersCollection object serialized in File System
     * @throws IOException if any error occurred
     * @throws ParseException if any error parsing the json file occurred
     */
    public static PlayersCollection getCollectionFromDisk() throws IOException, ParseException {
        final String path = getPath();

        final Serializator<PlayersCollection> ser = new SerializatorFactoryImpl().securePlayersCollectionSerializator();
        final Optional<PlayersCollection> optCollection = ser.deserialize(path, "testPlayerSer.json");
        if (optCollection.isPresent()) {
            return optCollection.get();
        } else {
            final PlayersCollection newCollection = new PlayersCollectionImpl();
            ser.serialize(path, newCollection, "testPlayerSer.json");
            return newCollection;
        }
    }

    /** This method is used to simplify the path obtainment.
     * @return Return the correct leaderboard file path if using Windows or Mac/Linux
     * where saving the Players data
     */
    private static String getPath() {
        String path;
        if (System.getProperty("os.name").contains("Windows")) {
            path = System.getenv("APPDATA") + System.getProperty("file.separator") + "BBTAN";
        } else {
            path = System.getenv("HOME") + System.getProperty("file.separator") + "BBTAN";
        }
        return path;
    }

    @Override
    public void addPlayer(final Player player) {
        if (containsPlayer(player.getName().toUpperCase())) {
            throw new IllegalStateException();
        }
        players.add(player);
    }

    @Override
    public void addPlayer(final String name) {
        if (containsPlayer(name.toUpperCase())) {
            throw new IllegalStateException();
        }
        final PlayerBuilder builder = new PlayerBuilderImpl().addNamePlayer(name.toUpperCase());
        this.players.add(builder.build());
    }

    @Override
    public Optional<List<Player>> getPlayers() {
        return Optional.of(this.players);
    }

    @Override
    public Optional<Player> getBestPlayer() {
        if (this.players.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(this.players.stream().max(Comparator.comparingInt(Player::getBestScore)).get());
    }

    @Override
    public Optional<Player> getPlayer(final String name) {
        if (this.containsPlayer(name.toUpperCase())) {
            return Optional.of(this.players.stream().filter(i -> i.getName().equals(name.toUpperCase())).findAny().get());
        }
        return Optional.empty();
    }

    @Override
    public Optional<Player> getCurrentPlayer() {
        if (this.currentPlayer.isEmpty()) {
            return Optional.empty();
        } else {
            return getPlayer(currentPlayer);
        }
    }

    @Override
    public void setCurrentPlayer(final String name) {
        this.currentPlayer = name.toUpperCase();
    }

    @Override
    public boolean containsPlayer(final String name) {
        if (this.players.isEmpty()) {
            return false;
        }

        return players.stream().map(i -> i.getName()).filter(i -> i.equals(name.toUpperCase())).count() > 0;
    }

    @Override
    public void logout() throws IOException {
        this.currentPlayer = "";
        this.update();
    }

    @Override
    public String getCurrentPlayerName() {
        if (this.currentPlayer.isEmpty()) {
            return "";
        }
        return currentPlayer;
    }

    @Override
    public void reset() {
        this.players = new ArrayList<>();
    }

    @Override
    public void setPlayers(final List<Player> list) {
        this.players = list;
    }

    @Override
    public void update() throws IOException {
        final String path = getPath();
        final Serializator<PlayersCollection> ser = new SerializatorFactoryImpl().securePlayersCollectionSerializator();
        ser.serialize(path, this, "testPlayerSer.json");
    }

    @Override
    public Optional<List<Player>> getPlayersInOrder() {
        if (this.players.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(this.players.stream().sorted((o1, o2) -> o2.getBestScore() - o1.getBestScore()).collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        return this.players.stream().map(i -> i.getName()).collect(Collectors.joining("-", "{", "}"));
    }

}
