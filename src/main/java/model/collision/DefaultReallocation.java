package model.collision;

import element.Point2D;
import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import java.util.Optional;

/**
 * Default class with the implementation of 2 important methods.
 * - getNewPositionOfBall
 * - getNewDirectionOfBall
 */
class DefaultReallocation implements Reallocation {

    @Override
    public final Point2D getNewPositionOfBall(final Vector2D penetration, final Point2D ballPosition,
                                              final Vector2D ballDirection) {
        final Optional<Vector2D> tmpPenetration = fixPenetration(penetration, ballDirection);
        if (tmpPenetration.isEmpty()) {
            return ballPosition;
        }
        final var err = ballDirection.scalarMultiplication(-tmpPenetration.get().getModulus());
        return new Point2DImpl(ballPosition.getX() + err.getXComponent(), ballPosition.getY() + err.getYComponent());
    }

    /**
     * If a ball have a collision with a block, not in angle,
     *     the penetration result is a 0 and a distance of penetration
     *     in x or y axis. With this method i fix the 0 value
     *     with the correct value with a simple proportion.
     */
    private Optional<Vector2D> fixPenetration(final Vector2D penetration, final Vector2D dirBall) {
        if (penetration.getXComponent() == 0) {
            if (penetration.getYComponent() == 0) {
                return Optional.empty();
            }
            return Optional.of(fixXComponent(penetration, dirBall));
        } else if (penetration.getYComponent() == 0) {
            return Optional.of(fixYComponent(penetration, dirBall));
        }
        return Optional.of(fixAnglePenetration(penetration, dirBall));
    }

    private Vector2D fixXComponent(final Vector2D penetration, final Vector2D dirBall) {
        double tmpX;
        if (dirBall.getYComponent() == 0) {
            tmpX = penetration.getYComponent() * dirBall.getXComponent();
        } else {
            tmpX = penetration.getYComponent() * dirBall.getXComponent() / dirBall.getYComponent();
        }
        return new Vector2DImpl(tmpX, penetration.getYComponent());
    }

    private Vector2D fixYComponent(final Vector2D penetration, final Vector2D dirBall) {
        double tmpY;
        if (dirBall.getXComponent() == 0) {
            tmpY = penetration.getXComponent() * dirBall.getYComponent();
        } else {
            tmpY = penetration.getXComponent() * dirBall.getYComponent() / dirBall.getXComponent();
        }
        return new Vector2DImpl(penetration.getXComponent(), tmpY);
    }

    private Vector2D fixAnglePenetration(final Vector2D penetration, final Vector2D dirBall) {
        if (Math.abs(dirBall.getXComponent()) < Math.abs(dirBall.getYComponent())) {
            return fixYComponent(penetration, dirBall);
        } else {
            return fixXComponent(penetration, dirBall);
        }
    }

    @Override
    public final Vector2D getNewDirectionOfBall(final Vector2D penetration, final Vector2D ballDirection) {
        if (penetration.getXComponent() == penetration.getYComponent()) {
            //angle!!
            return ballDirection.scalarMultiplication(-1);
        }
        if (penetration.getXComponent() == 0) {
            //y
            return easyVectorMultiplication(ballDirection, 1, -1);
        }
        if (penetration.getYComponent() == 0) {
            //x
            return easyVectorMultiplication(ballDirection, -1, 1);
        }
        if (penetration.getXComponent() > penetration.getYComponent()) {
            //y
            return easyVectorMultiplication(ballDirection, 1, -1);
        }
        if (penetration.getXComponent() < penetration.getYComponent()) {
            //x
            return easyVectorMultiplication(ballDirection, -1, 1);
        }
        return null;
    }

    private Vector2D easyVectorMultiplication(final Vector2D dir, final int xComponent, final int yComponent) {
        return dir.vectorMultiplication(new Vector2DImpl(xComponent, yComponent));
    }
}
