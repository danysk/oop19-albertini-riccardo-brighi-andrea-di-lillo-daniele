package model.collision;

import javafx.scene.shape.Shape;
import java.util.List;

/**
 * Interface to manage the collisions with blocks.
 */
interface CollisionBlocks extends Collision {

    /**
     * Method to update all current Blocks.
     *
     * @param blocks all new current blocks
     */
    void updateBlocks(List<Shape> blocks);
}
