package model.collision;

import element.Point2D;
import element.Vector2D;
import element.Vector2DImpl;
import model.ball.Ball;
import java.util.Optional;

/**
 * Class to implement all collision with borders.
 */
final class CollisionBordersImpl extends DefaultReallocation implements CollisionBorders {
    private static final double PRECISION = .0000001;
    private final double top;
    private final double left;
    private final double bottom;
    private final double right;

    /**
     * Constructor of BorderCollision.
     *
     * @param topLeft the point that represent top left angle
     * @param bottomRight the point that represent bottom right angle
     */
    CollisionBordersImpl(final Point2D topLeft, final Point2D bottomRight) {
        this.top = topLeft.getX();
        this.left = topLeft.getY();
        this.bottom = bottomRight.getX();
        this.right = bottomRight.getY();
    }

    @Override
    public synchronized Optional<CollisionDetected> checkCollision(final Ball ball, final Point2D relocatedPosition) {
        if (Math.abs(relocatedPosition.getY() + ball.getRadius() - top) < PRECISION) {
            return this.collisionWithBottomBorder(relocatedPosition);
        }
        Vector2D newDirection = ball.getDirection();
        if (Math.abs(relocatedPosition.getY() - ball.getRadius() - bottom) < PRECISION) {
            System.out.println("Collision Top Border");
            newDirection = new Vector2DImpl(newDirection.getXComponent(), newDirection.getYComponent() * -1);
        }
        if (Math.abs(relocatedPosition.getX() + ball.getRadius() - left) < PRECISION
                || Math.abs(relocatedPosition.getX() - ball.getRadius() - right) < PRECISION) {
            System.out.println("Collision Lateral Border");
            newDirection = new Vector2DImpl(newDirection.getXComponent() * -1, newDirection.getYComponent());
        }
        return Optional.of(new CollisionDetectedImpl(newDirection, relocatedPosition));
    }

    private Optional<CollisionDetected> collisionWithBottomBorder(final Point2D newPosition) {
        System.out.println("Collision Bottom Border");
        return Optional.of(new CollisionDetectedImpl(newPosition));
    }

    /**
     * Relocate the ball if is out of bounds.
     *
     * @param ball current ball
     * @return the new position (where was the collision) or the his position
     */
    @Override
    public Point2D relocateBall(final Ball ball) {
        return super.getNewPositionOfBall(getPenetration(ball.getPosition(), ball.getRadius()), ball.getPosition(),
                ball.getDirection());
    }

    @Override
    public boolean checkBallInField(final Ball ball) {
        return !(checkBallOutYComponent(ball.getPosition(), ball.getRadius())
                || checkBallOutXComponent(ball.getPosition(), ball.getRadius()));
    }

    /**
     * Method to check if the ball is out of top or bottom.
     *
     * @param posBall position of current ball
     * @param radius radius of current ball
     * @return true or false is the ball is out of bottom or top
     */
    private boolean checkBallOutYComponent(final Point2D posBall, final double radius) {
        return posBall.getY() - radius <= bottom || posBall.getY() + radius >= top;
    }

    /**
     * Method to check if the ball is out of left or right.
     *
     * @param posBall position of current ball
     * @param radius radius of current ball
     * @return true or false is the ball is out of right or left
     */
    private boolean checkBallOutXComponent(final Point2D posBall, final double radius) {
        return posBall.getX() - radius <= right || posBall.getX() + radius >= left;
    }

    private Vector2D getPenetration(final Point2D pos, final double radius) {
        Vector2D penetration = new Vector2DImpl(0, 0);
        if (checkBallOutYComponent(pos, radius)) { //bottom top
            if (pos.getY() - radius < bottom) {
                penetration = new Vector2DImpl(0, getDistancePlusRadius(pos.getY(), bottom, radius));
            } else if (pos.getY() + radius > top) {
                penetration = new Vector2DImpl(0, getDistancePlusRadius(pos.getY(), top, -radius));
            }
        } else if (checkBallOutXComponent(pos, radius)) { //dx sx
            if (pos.getX() - radius < right) {
                penetration = new Vector2DImpl(getDistancePlusRadius(pos.getX(), right, radius), penetration.getYComponent());
            } else if (pos.getX() + radius > left) {
                penetration = new Vector2DImpl(getDistancePlusRadius(pos.getX(), left, -radius), penetration.getYComponent());
            }
        }
        return penetration;
    }

    private double getDistancePlusRadius(final double from, final double to, final double radius) {
        return Math.abs(from - to - radius);
    }
}
