package model.items;

import java.util.Set;

/**
 * This interface model a generic inventory as a collection of items and
 * the currently used item by a certain player.
 *
 * @param <X> X is any kind of item of the inventory you want to create
 */
public interface UnlockedItems<X> {

    /**
     * This method add new element to the items inventory.
     *
     * @param newElement Is the new element to add
     */
    void addItem(X newElement);

    /**
     * @return Return the current item used in the inventory
     */
    X getCurrentItem();

    /**
     * @return Return the set of items unlocked in the inventory
     */
    Set<X> getItems();

    /**
     * This method check whether the item is contained or not in the inventory.
     *
     * @param item The item to check if contained
     * @return Return true is item is contained, false otherwise
     */
    boolean checkItem(X item);

    /**
     * This method set the current item.
     *
     * @param item The item is set as current item used
     * @throws IllegalArgumentException Throws an exception if item is not contained in the inventory
     */
    void setCurrentItem(X item);
}
