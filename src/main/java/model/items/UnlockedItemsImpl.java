package model.items;

import java.util.HashSet;
import java.util.Set;

/**
 * This class implements the UnlockedItemsImpl
 * @param <X> The X is the generic type.
 */
public final class UnlockedItemsImpl<X> implements UnlockedItems<X> {

    /**
     * This is the current element currently used by the player in this inventory.
     */
    private X currentElement;

    /**
     * This is the values set unlocked by the user for this item.
     */
    private final Set<X> unlockedItems;

    public UnlockedItemsImpl(final X currentElement) {
        unlockedItems = new HashSet<>();
        addItem(currentElement);
    }

    public UnlockedItemsImpl(final X currentElement, final Set<X> unlockedItems) {
        this.currentElement = currentElement;
        this.unlockedItems = unlockedItems;
    }

    @Override
    public void addItem(final X newElement) {
        this.unlockedItems.add(newElement);
        this.currentElement = newElement;
    }

    @Override
    public X getCurrentItem() {
        return this.currentElement;
    }

    @Override
    public Set<X> getItems() {
        return this.unlockedItems;
    }

    @Override
    public boolean checkItem(final X item) {
        return this.unlockedItems.contains(item);
    }

    @Override
    public void setCurrentItem(final X item) {
        if (checkItem(item)) {
            this.currentElement = item;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
