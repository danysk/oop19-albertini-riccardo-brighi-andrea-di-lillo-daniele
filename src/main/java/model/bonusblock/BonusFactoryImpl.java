package model.bonusblock;

import element.Point2D;
import org.tinylog.Logger;

import java.util.Map;
import java.util.Objects;
import java.util.Random;

/**
 * Implementation of the BonusFactory.
 */
public class BonusFactoryImpl implements BonusFactory {

    private final Map<BonusType, Strategy<BonusBlock>> map;

    /**
     * Constructor of the BonusFactoryImpl.
     *
     * @param mapPassed the map which contains the Strategy interface.
     */
    public BonusFactoryImpl(final Map<BonusType, Strategy<BonusBlock>> mapPassed) {
        this.map = mapPassed;
    }

    /**
     * @param pos the position of the bonus.
     * @return a BonusBlock random.
     */
    @Override
    public BonusBlock random(final Point2D pos) {
        legalPosition(pos);
        return createBonus(BonusType.getBonusByIndex(new Random().nextInt(BonusType.values().length)), pos);
    }

    /**
     * @param pos the position of the bonus.
     * @return a BonusBlock {@link BonusFactory.BonusType#COIN}
     */
    @Override
    public BonusBlock coin(final Point2D pos) {
        legalPosition(pos);
        return createBonus(BonusType.COIN, pos);
    }

    /**
     * @param pos the position of the bonus.
     * @return a BonusBlock {@link BonusFactory.BonusType#ADDBALL}.
     */
    @Override
    public BonusBlock addBall(final Point2D pos) {
        legalPosition(pos);
        return createBonus(BonusType.ADDBALL, pos);
    }

    /**
     * @param pos the position of the bonus.
     * @return a a BonusBlock {@link BonusFactory.BonusType#HORIZONTAL}.
     */
    @Override
    public BonusBlock horizontal(final Point2D pos) {
        legalPosition(pos);
        return createBonus(BonusType.HORIZONTAL, pos);
    }

    /**
     * @param pos the position of the bonus.
     * @return a BonusBlock {@link BonusFactory.BonusType#VERTICAL}.
     */
    @Override
    public BonusBlock vertical(final Point2D pos) {
        legalPosition(pos);
        return createBonus(BonusType.VERTICAL, pos);
    }

    private BonusBlock createBonus(final BonusType b, final Point2D pos) {

        return new BonusBlock() {

            private Point2D position = pos;
            private final BonusType type = b;
            private boolean hitted = false;
            private final Strategy<BonusBlock> strategy = map.get(b);

            @Override
            public void setPosition(final Point2D p) {
                this.position = p;
            }

            @Override
            public Point2D getPosition() {
                return this.position;
            }

            @Override
            public double getRadius() {
                return this.type.getRadius();
            }

            @Override
            public boolean isInstatDelete() {
                return this.type.isInstatDelete();
            }

            @Override
            public String getImg() {
                return this.type.getImg();
            }

            @Override
            public boolean isHitted() {
                return hitted;
            }

            @Override
            public boolean hit() {
                this.strategy.doHit(this);
                this.hitted = true;
                return this.isInstatDelete();
            }
        };
    }

    private void legalPosition(final Point2D pos) {
        if (Objects.isNull(pos)) {
            Logger.error("Posizione del blocco bonus non valida.");
            throw new IllegalStateException();
        }
    }
}
