package model.bonusblock;

import element.Point2D;

/**
 * The factory for the creating of the bonus.
 */
public interface BonusFactory {

    /**
     * Bonus that can be created.
     */
    enum BonusType implements BonusEnum {
        /**
         * Coin bonus.
         */
        COIN(15, "coin", true),

        /**
         * Add an additional ball, bonus.
         */
        ADDBALL(10, "add_ball", true),

        /**
         * hit the row of the block.
         */
        HORIZONTAL(10, "horizontal_row (1)", false),

        /**
         * hiw the column of the block.
         */
        VERTICAL(10, "vertical-row", false);

        private final double radius;
        private final String img;
        private final boolean instatDelete;

        /**
         *
         * @param radius the radius of the bonus.
         * @param img the name of the img.
         * @param instatDelete if the bonus is deleted after being hit once.
         */
        BonusType(final double radius, final String img, final boolean instatDelete) {
            this.radius = radius;
            this.img = img;
            this.instatDelete = instatDelete;
        }

        /**
         * Return the {@link BonusFactory.BonusType} of the specified at @param.
         *
         * @param index index corresponding to a specific {@link BonusFactory.BonusType}.
         * @return a {@link BonusFactory.BonusType} based on the @param.
         */
        public static BonusType getBonusByIndex(final int index) {
            switch (index) {
                case 0:
                    return COIN;
                case 1:
                    return ADDBALL;
                case 2:
                    return HORIZONTAL;
                case 3:
                    return VERTICAL;
                default:
                    throw new IndexOutOfBoundsException();
            }
        }

        @Override
        public double getRadius() {
            return this.radius;
        }

        @Override
        public String getImg() {
            return this.img;
        }

        @Override
        public boolean isInstatDelete() {
            return this.instatDelete;
        }
    }

    /**
     * Return {@link BonusBlock} with random {@link BonusFactory.BonusType}.
     *
     * @param pos the position of the bonus.
     * @return The {@link BonusBlock} created.
     */
    BonusBlock random(Point2D pos);

    /**
     * Return a {@link BonusBlock} of type {@link BonusFactory.BonusType#COIN}.
     *
     * @param pos the position of the bonus.
     * @return The {@link BonusBlock} created.
     */
    BonusBlock coin(Point2D pos);

    /**
     * Return a {@link BonusBlock} of {@link BonusFactory.BonusType#ADDBALL}.
     *
     * @param pos the position of the bonus.
     * @return The {@link BonusBlock} created.
     */
    BonusBlock addBall(Point2D pos);

    /**
     * Return a {@link BonusBlock} of {@link BonusFactory.BonusType#HORIZONTAL}.
     *
     * @param pos the position of the bonus.
     * @return The {@link BonusBlock} created.
     */
    BonusBlock horizontal(Point2D pos);

    /**
     * Return a {@link BonusBlock} of {@link BonusFactory.BonusType#VERTICAL}.
     *
     * @param pos the position of the bonus.
     * @return The {@link BonusBlock} created.
     */
    BonusBlock vertical(Point2D pos);

}
