package utility;

public abstract class GameThreadImpl extends Thread {

    private volatile boolean isPaused;
    private volatile boolean isOver;
    private volatile boolean gameOver;
    private boolean isStarted;

    public GameThreadImpl() {
        isPaused = false;
        isOver = true;
        gameOver = false;
        isStarted = false;
    }

    /**
     * This method is used to start the thread.
     */
    @Override
    public synchronized void start() {
        this.isStarted = true;
        this.isOver = false;
        if (!this.isAlive()) {
            super.start();
        } else {
            this.resumeGame();
        }
    }

    /**
     * This method is used to verify if thread is started or not.
     * @return Return true if started, false otherwise
     */
    public boolean isStarted() {
        return this.isStarted;
    }

    /**
     * This method is used to resume thread after pausing it.
     */
    public synchronized void resumeGame() {
        if (this.isOver) {
            this.isOver = false;
        }
        this.isPaused = false;
        this.notifyAll();
    }

    /**
     * This method is used to verify if thread is started or not.
     */
    public synchronized void pauseGame() {
        this.isPaused = true;
        this.interrupt();
    }

    /**
     * This method is used to put the thread into sleeping state.
     */
    public synchronized void waitEndPause() {
        this.isOver = true;
        while (this.isOver) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is used to terminate the thread.
     */
    public synchronized void endThread() {
        this.gameOver = true;
        this.interrupt();
        this.notifyAll();
    }

    /**
     * This method is used to verify if thread is ended or not.
     * @return Return true if ended, false otherwise.
     */
    public synchronized boolean isEndGame() {
        return this.gameOver;
    }
    /**
     * This method is used to verify if thread is paused or not.
     * @return Return true if paused, false otherwise.
     */
    public synchronized boolean isPause() {
        return this.isPaused;
    }
}
