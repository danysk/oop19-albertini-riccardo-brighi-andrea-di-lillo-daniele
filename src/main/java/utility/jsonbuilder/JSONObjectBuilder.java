package utility.jsonbuilder;

import org.json.simple.JSONObject;

/**
 * This utility simplify a json object creation using the SimpleJson library.
 */
public interface JSONObjectBuilder {

    /**
     * @param name Is the name of the property that can be add
     * @param o    Is the object associated to the name
     * @return Return the builder for new pair of object
     */
    JSONObjectBuilder put(String name, Object o);

    /**
     * @return Return the JSONObject build
     */
    JSONObject build();
}
