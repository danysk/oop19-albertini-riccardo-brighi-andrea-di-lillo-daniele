package utility;

/**
 * Interface for counter with generic.
 *
 * @param <T> type of value of counter
 */
public interface Counter<T extends Number> {

    /**
     * @return the actual value of the counter
     */
    T getValue();

    /**
     * @return the step value of the counter
     */
    T getStep();

    /**
     * @return the start value of the counter
     */
    T getStart();

    /**
     * increment of one unit the counter.
     */

    void increment();

    /**
     * reset the counter to the start value.
     */

    void reset();

}
