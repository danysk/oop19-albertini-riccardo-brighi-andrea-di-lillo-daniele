package utility;

/**
 * Factory for the counter of double.
 *
 */
public class DoubleCounterFactory extends AbstractCounterFactory<Double> implements CounterFactory<Double> {

    private static final double START_STANDARD = 0.0;
    private static final double STEP_STANDARD = 0.1;

    /**
     * standard double counter.
     */
    @Override
    public final Counter<Double> standardCounter() {
        return this.standardCounterFromValue(START_STANDARD);
    }

    /**
     * standard double counter from value.
     */
    @Override
    public final Counter<Double> standardCounterFromValue(final Double startValue) {
        return this.counterFromValueWithStep(startValue, STEP_STANDARD);
    }

    /**
     * standard double counter with step.
     */
    @Override
    public final Counter<Double> counterWithStep(final Double step) {
        final double start = 0.0;
        return this.counterFromValueWithStep(start, step);
    }

    /**
     * standard double counter from value with step.
     */
    @Override
    public final Counter<Double> counterFromValueWithStep(final Double startValue, final Double step) {
        return this.counterFromValueWithStep(startValue, step, Double::sum);
    }
}
