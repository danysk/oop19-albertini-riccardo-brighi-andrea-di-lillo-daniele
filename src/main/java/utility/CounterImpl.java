package utility;

/**
 * use CounterFactoryInteger instead.
 */
@Deprecated
public final class CounterImpl implements Counter<Integer> {

    private final int start;
    private int value;

    /**
     * create a counter that start from 0.
     */
    public CounterImpl() {
        this(0);
    }

    /**
     * @param start the number to start counting
     *              <p>
     *              create a counter that start from @param start.
     *              </p>
     */

    public CounterImpl(final int start) {
        this.start = start;
        this.reset();
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public Integer getStep() {
        return 1;
    }

    @Override
    public Integer getStart() {
        return start;
    }

    @Override
    public void increment() {
        this.value++;
    }

    @Override
    public void reset() {
        this.value = this.start;
    }

}

