package utility;

/**
 * Factory for the limitCounter.
 */
public class LimitCounterFactoryImpl implements LimitCounterFactory {

    /**
     * @param counter the counter to use
     * @param limit   the limit of the counter
     * @param <T>     the type of the counter
     * @return a counter that is limited to @param limit
     */
    @Override
    public <T extends Number & Comparable<T>> Counter<T> limitCounter(final Counter<T> counter, final T limit) {
        return this.limitCounterNotify(counter, limit, false);
    }

    /**
     * @param counter the counter to use
     * @param limit   the limit of the counter
     * @param <T>     the type of the counter
     * @return a counter that is limited to @param limit notify and then reset.
     */
    @Override
    public <T extends Number & Comparable<T>> CounterWithEvents<T> limitCounterNotifyAndReset(final Counter<T> counter,
                                                                                              final T limit) {
        return this.limitCounterNotify(counter, limit, true);
    }

    /**
     * @param counter       the counter to use
     * @param limit         the limit of the counter
     * @param limitObserver the observer to add
     * @param <T>           the type of the counter
     * @return a Counter that is limited and that notify when is reach then reset
     */
    @Override
    public <T extends Number & Comparable<T>> CounterWithEvents<T> limitCounterNotifyAndReset(final Counter<T> counter,
                                                                                              final T limit, final Observer<T> limitObserver) {
        final CounterWithEvents<T> counterWithEvents = this.limitCounterNotifyAndReset(counter, limit);
        counterWithEvents.addObserver(limitObserver);
        return counterWithEvents;
    }

    private static final class CounterWithEventsImpl<T extends Number & Comparable<T>> extends SourceImpl<T>
            implements CounterWithEvents<T> {
        private final Counter<T> counter;
        private final T limit;
        private final int initialComparison;
        private final boolean reset;

        private CounterWithEventsImpl(final Counter<T> counter, final T limit, final int initialComparison,
                                      final boolean reset) {
            this.counter = counter;
            this.limit = limit;
            this.initialComparison = initialComparison;
            this.reset = reset;
        }

        @Override
        public T getValue() {
            return this.counter.getValue().compareTo(this.limit) < 0 ? this.counter.getValue() : this.limit;
        }

        @Override
        public T getStep() {
            return this.counter.getStep();
        }

        @Override
        public T getStart() {
            return this.counter.getStart();
        }

        @Override
        public void increment() {
            if (limit.compareTo(counter.getValue()) == initialComparison) {
                counter.increment();
                if (limit.compareTo(counter.getValue()) == -initialComparison) {
                    this.notifyObservers(this.getValue());
                    if (this.reset) {
                        this.reset();
                    }
                }
            }
        }

        @Override
        public void reset() {
            this.counter.reset();
        }
    }

    private <T extends Number & Comparable<T>> CounterWithEvents<T> limitCounterNotify(final Counter<T> counter,
                                                                                       final T limit, final boolean reset) {
        final int initialComparation = limit.compareTo(counter.getStart());
        if (initialComparation == -limit.compareTo(counter.getValue())
                || initialComparation != Math.signum(counter.getStep().doubleValue())) {
            throw new IllegalArgumentException();
        }
        return new CounterWithEventsImpl<>(counter, limit, initialComparation, reset);
    }
}
