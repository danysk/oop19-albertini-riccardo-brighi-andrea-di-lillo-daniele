package utility;

import java.util.HashSet;
import java.util.Set;

/**
 * The class for the Observer Pattern.
 *
 * @param <T> the type of the value of the SourceImpl
 */
public class SourceImpl<T> implements Source<T> {

    private final Set<Observer<? super T>> observers = new HashSet<>();

    @Override
    public final void addObserver(final Observer<? super T> observer) {
        this.observers.add(observer);
    }

    @Override
    public final void removeObserver(final Observer<? super T> observer) {
        this.observers.remove(observer);
    }

    @Override
    public final Set<Observer<? super T>> getObserversSet() {
        return this.observers;
    }

    @Override
    public final void notifyObservers(final T arg) {
        this.observers.forEach(i -> i.update(this, arg));
    }
}
